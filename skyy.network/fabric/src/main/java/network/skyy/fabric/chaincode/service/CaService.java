package network.skyy.fabric.chaincode.service;

public interface CaService {

    String getCaUrl(int orgId);

}
