package network.skyy.fabric.chaincode.service.impl;

import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.service.OrdererService;
import network.skyy.network.Config;
import network.skyy.network.FabricNetworkConfig;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class OrdererServiceImpl implements OrdererService {

    private static final String ORDERER_URL = "grpcs://" + Config.HOST + ":7050";
    private static final String ORDERER_NAME = "orderer." + Config.DOMAIN;


    @Override
    public Orderer newOrderer(FabricClient context) throws InvalidArgumentException {
        Properties ordererProperties = new Properties();

        ordererProperties.setProperty("pemFile", FabricNetworkConfig.getOrdererCertPath());
        ordererProperties.setProperty("trustServerCertificate", "true");
        ordererProperties.setProperty("hostnameOverride", ORDERER_NAME);
        ordererProperties.setProperty("sslProvider", "openSSL");
        ordererProperties.setProperty("negotiationType", "TLS");
        ordererProperties.put("grpc.NettyChannelBuilderOption.keepAliveTime", new Object[]{5L, TimeUnit.MINUTES});
        ordererProperties.put("grpc.NettyChannelBuilderOption.keepAliveTimeout", new Object[]{8L, TimeUnit.SECONDS});

        return context.getClient().newOrderer(ORDERER_NAME, ORDERER_URL, ordererProperties);
    }

}
