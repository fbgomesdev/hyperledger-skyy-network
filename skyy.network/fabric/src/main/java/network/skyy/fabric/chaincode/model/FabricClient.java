package network.skyy.fabric.chaincode.model;

import lombok.Builder;
import lombok.Data;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.security.CryptoSuite;


@Data
public class FabricClient {

    private HFClient client;

    private User userContext;


    @Builder
    public FabricClient(User userContext) throws Exception {
        client = HFClient.createNewInstance();
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        client.setCryptoSuite(cryptoSuite);
        this.setUserContext(userContext);
    }

    public void setUserContext(User userContext) throws Exception {
        this.userContext = userContext;
        this.client.setUserContext(userContext);
    }


}
