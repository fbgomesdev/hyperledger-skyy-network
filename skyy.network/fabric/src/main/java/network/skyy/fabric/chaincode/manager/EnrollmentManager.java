
package network.skyy.fabric.chaincode.manager;

import network.skyy.fabric.chaincode.model.FabricUserContext;
import network.skyy.fabric.chaincode.service.FabricServiceProvider;
import network.skyy.fabric.user.UserContext;
import network.skyy.fabric.util.Util;
import network.skyy.network.Config;
import network.skyy.network.FabricNetworkConfig;

public class EnrollmentManager {

    private static EnrollmentManager enrollmentManager = new EnrollmentManager();

    public static EnrollmentManager instance() {
        return enrollmentManager;
    }


    public boolean registerEnrollUser() {
        try {
            Util.cleanUp();
            int orgId = 1;
            String caUrl = FabricServiceProvider.CA_SERVICE.getCaUrl(orgId);
            String orgName = FabricNetworkConfig.getOrgName(orgId);


            // Enroll Admin to Org1MSP
            UserContext adminUserContext = new UserContext();
            adminUserContext.setName(Config.ADMIN);
            adminUserContext.setAffiliation(orgName);


            adminUserContext.setMspId(FabricNetworkConfig.getOrgMSP(orgId));


            FabricUserContext fabricAdminContext = FabricUserContext.builder().userContext(adminUserContext).build();

            FabricServiceProvider.ENROLLMENT_SERVICE.enrollAdminUser(fabricAdminContext, caUrl, Config.ADMIN, Config.ADMIN_PASSWORD);

            // Register and Enroll user to Org1MSP
            UserContext userContext = new UserContext();
            String name = "user" + System.currentTimeMillis();
            userContext.setName(name);
            userContext.setAffiliation(orgName);
            userContext.setMspId(FabricNetworkConfig.getOrgMSP(orgId));

            String eSecret = FabricServiceProvider.ENROLLMENT_SERVICE.registerUser(fabricAdminContext, caUrl, name, orgName);


            FabricUserContext fabricUserContext = FabricUserContext.builder().userContext(userContext).build();
            FabricServiceProvider.ENROLLMENT_SERVICE.enrollUser(fabricAdminContext, caUrl, fabricUserContext, eSecret);


        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
