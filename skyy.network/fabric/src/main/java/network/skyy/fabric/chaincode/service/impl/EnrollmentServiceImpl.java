/****************************************************** 
 *  Copyright 2018 IBM Corporation 
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  you may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at 
 *  http://www.apache.org/licenses/LICENSE-2.0 
 *  Unless required by applicable law or agreed to in writing, software 
 *  distributed under the License is distributed on an "AS IS" BASIS, 
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *  See the License for the specific language governing permissions and 
 *  limitations under the License.
 */
package network.skyy.fabric.chaincode.service.impl;

import network.skyy.fabric.chaincode.model.FabricUserContext;
import network.skyy.fabric.chaincode.service.EnrollmentService;
import network.skyy.fabric.user.UserContext;
import network.skyy.fabric.util.Util;
import network.skyy.network.FabricNetworkConfig;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;

import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EnrollmentServiceImpl implements EnrollmentService {


    @Override
    public FabricUserContext enrollAdminUser(FabricUserContext fabricAdminContext, String caUrl, String username, String password) throws Exception {


        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        Properties caProperties = getCaProperties();


        HFCAClient instance = HFCAClient.createNewInstance(caUrl, caProperties);
        instance.setCryptoSuite(cryptoSuite);

        UserContext adminContext = fabricAdminContext.getUserContext();

        UserContext userContext = Util.readUserContext(adminContext.getAffiliation(), username);
        if (userContext != null) {
            Logger.getLogger(EnrollmentServiceImpl.class.getName()).log(Level.WARNING, "CA -" + caUrl + " admin is already enrolled.");
            return FabricUserContext.builder().userContext(userContext).build();
        }

        Enrollment adminEnrollment = instance.enroll(username, password);
        adminContext.setEnrollment(adminEnrollment);
        Logger.getLogger(EnrollmentServiceImpl.class.getName()).log(Level.INFO, "CA -" + caUrl + " Enrolled Admin.");
        Util.writeUserContext(adminContext);


        return FabricUserContext.builder().userContext(adminContext).build();
    }

    @Override
    public FabricUserContext enrollUser(FabricUserContext fabricAdminContext, String caUrl, FabricUserContext fabricUserContext, String secret) throws Exception {


        UserContext user = fabricUserContext.getUserContext();
        UserContext adminContext = fabricAdminContext.getUserContext();
        Properties caProperties = getCaProperties();

        HFCAClient instance = HFCAClient.createNewInstance(caUrl, caProperties);
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        instance.setCryptoSuite(cryptoSuite);

        UserContext userContext = Util.readUserContext(adminContext.getAffiliation(), user.getName());
        if (userContext != null) {
            Logger.getLogger(EnrollmentServiceImpl.class.getName()).log(Level.WARNING, "CA -" + caUrl + " User " + user.getName() + " is already enrolled");
            return FabricUserContext.builder().userContext(userContext).build();
        }
        Enrollment enrollment = instance.enroll(user.getName(), secret);
        user.setEnrollment(enrollment);
        Util.writeUserContext(user);
        Logger.getLogger(EnrollmentServiceImpl.class.getName()).log(Level.INFO, "CA -" + caUrl + " Enrolled User - " + user.getName());

        return FabricUserContext.builder().userContext(user).build();
    }


    @Override
    public String registerUser(FabricUserContext fabricAdminContext, String caUrl, String username, String organization) throws Exception {

        UserContext adminContext = fabricAdminContext.getUserContext();
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();

        Properties caProperties = getCaProperties();


        HFCAClient instance = HFCAClient.createNewInstance(caUrl, caProperties);
        instance.setCryptoSuite(cryptoSuite);


        UserContext userContext = Util.readUserContext(adminContext.getAffiliation(), username);
        if (userContext != null) {
            Logger.getLogger(EnrollmentServiceImpl.class.getName()).log(Level.WARNING, "CA -" + caUrl + " User " + username + " is already registered.");
            return null;
        }
        RegistrationRequest rr = new RegistrationRequest(username, organization);
        String enrollmentSecret = instance.register(rr, adminContext);
        Logger.getLogger(EnrollmentServiceImpl.class.getName()).log(Level.INFO, "CA -" + caUrl + " Registered User - " + username);
        return enrollmentSecret;
    }

    private Properties getCaProperties() {

        // TODO move in util !
        Properties caProperties = new Properties();
        String cert = FabricNetworkConfig.getCACertPath(1);
        File cf = new File(cert);
        if (!cf.exists() || !cf.isFile()) {
            throw new RuntimeException("TEST is missing cert file " + cf.getAbsolutePath());
        }
        caProperties.setProperty("pemFile", cf.getAbsolutePath());
        //testing environment only NOT FOR PRODUCTION!
        caProperties.setProperty("allowAllHostNames", "true");
        return caProperties;
    }


}
