package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.model.FabricChannel;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;

import java.util.Collection;

public interface ChaincodeService {

    Collection<ProposalResponse> deployChainCode(
            FabricClient context,
            String chainCodeName,
            String chaincodePath,
            String codepath,
            String language,
            String version,
            Collection<Peer> peers) throws Exception;


    Collection<ProposalResponse> instantiateChainCode(
            FabricChannel channel,
            String chaincodeName,
            String version,
            String chaincodePath,
            String language,
            String functionName,
            String[] functionArgs,
            String policyPath) throws Exception;


    Collection<ProposalResponse> invokeRead(
            FabricChannel channel,
            String chaincodeName,
            String functionName, String[] args) throws Exception;


    Collection<ProposalResponse> invokeWrite(
            FabricChannel channel,
            String contract,
            String function,
            String[] arguments) throws Exception;
}
