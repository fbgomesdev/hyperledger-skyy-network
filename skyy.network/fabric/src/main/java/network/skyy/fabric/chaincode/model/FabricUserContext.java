package network.skyy.fabric.chaincode.model;

import lombok.Builder;
import lombok.Data;
import network.skyy.fabric.user.UserContext;

@Data
@Builder
public class FabricUserContext {

    private UserContext userContext;
}
