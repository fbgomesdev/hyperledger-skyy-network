package network.skyy.fabric.chaincode.model;

import lombok.Builder;
import lombok.Data;
import org.hyperledger.fabric.sdk.Channel;


@Data
@Builder
public class FabricChannel {

    private Channel channel;

    private FabricClient context;

}
