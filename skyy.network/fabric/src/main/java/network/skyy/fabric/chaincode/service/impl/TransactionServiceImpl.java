package network.skyy.fabric.chaincode.service.impl;


import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.service.FabricServiceProvider;
import network.skyy.fabric.chaincode.service.TransactionService;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransactionServiceImpl implements TransactionService {

    /**
     * Send transaction proposal.
     *
     * @param request
     * @return
     * @throws ProposalException
     * @throws InvalidArgumentException
     */
    @Override
    public Collection<ProposalResponse> sendTransactionProposal(
            String channelName,
            FabricClient context, TransactionProposalRequest request)
            throws ProposalException, InvalidArgumentException {

        Channel channel = FabricServiceProvider.CHANNEL_SERVICE.connectToChannel(channelName, context).getChannel();


        Logger.getLogger(EnrollmentServiceImpl.class.getName()).log(Level.INFO,
                "Sending transaction proposal on channel " + channel.getName());

        Collection<ProposalResponse> response = channel.sendTransactionProposal(request, channel.getPeers());
        for (ProposalResponse pres : response) {
            String stringResponse = new String(pres.getChaincodeActionResponsePayload());
            Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.INFO,
                    "Transaction proposal on channel " + channel.getName() + " " + pres.getMessage() + " "
                            + pres.getStatus() + " with transaction id:" + pres.getTransactionID());
            Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.INFO, stringResponse);
        }

        CompletableFuture<BlockEvent.TransactionEvent> cf = channel.sendTransaction(response);
        Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.INFO, cf.toString());

        return response;
    }


    /**
     * Query a transaction by id.
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    @Override
    public TransactionInfo queryByTransactionId(
            String channelName,
            FabricClient context,
            String txnId) throws Exception {

        Channel channel = FabricServiceProvider.CHANNEL_SERVICE.connectToChannel(channelName, context).getChannel();

        Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.INFO,
                "Querying by trasaction id " + txnId + " on channel " + channel.getName());
        Collection<Peer> peers = channel.getPeers();
        for (Peer peer : peers) {
            TransactionInfo info = channel.queryTransactionByID(peer, txnId);
            return info;
        }
        return null;
    }

}
