package network.skyy.fabric.chaincode.service.impl;

import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.model.FabricPeer;
import network.skyy.fabric.chaincode.service.PeerService;
import network.skyy.network.Config;
import network.skyy.network.FabricNetworkConfig;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PeerServiceImpl implements PeerService {


    private static final Map<String, String> URL_MAP = new HashMap<String, String>() {{
        put("ORG1_PEER_0", "grpcs://" + Config.HOST + ":7051");
        put("ORG1_PEER_1", "grpcs://" + Config.HOST + ":7056");
        put("ORG2_PEER_0", "grpcs://" + Config.HOST + ":8051");
        put("ORG2_PEER_1", "grpcs://" + Config.HOST + ":8056");
    }};


    @Override
    public FabricPeer newPeer(FabricClient context, int organization, int peer) throws InvalidArgumentException {

        HFClient hfClient = context.getClient();
        String name = getPeerName(organization, peer);
        String url = getPeerUrl(organization, peer);

        Properties peerProperties = new Properties();
        peerProperties.setProperty("pemFile", FabricNetworkConfig.getPeerCertPath(organization, peer));
        peerProperties.setProperty("trustServerCertificate", "true"); //testing environment only NOT FOR PRODUCTION!
        peerProperties.setProperty("hostnameOverride", name);
        peerProperties.setProperty("sslProvider", "openSSL");
        peerProperties.setProperty("negotiationType", "TLS");
        peerProperties.put("grpc.NettyChannelBuilderOption.maxInboundMessageSize", 9000000);


        Peer peerInstance = hfClient.newPeer(name, url, peerProperties);

        return FabricPeer.builder()
                .peer(peerInstance)
                .context(context)
                .build();

    }

    @Override
    public String getPeerUrl(int organization, int peer) {
        String key = "ORG" + organization + "_PEER_" + peer;
        return URL_MAP.get(key);
    }

    @Override
    public String getPeerName(int organization, int peer) {
        return "peer" + peer + ".org" + organization + "." + Config.DOMAIN;
    }

}
