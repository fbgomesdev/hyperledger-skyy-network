
package network.skyy.fabric.chaincode.manager;

import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.model.FabricChannel;
import network.skyy.fabric.chaincode.service.FabricServiceProvider;
import network.skyy.fabric.user.UserContext;
import network.skyy.network.Config;
import org.hyperledger.fabric.sdk.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChaincodeManager {


    private static ChaincodeManager chaincodeManager = new ChaincodeManager();

    public static ChaincodeManager instance() {
        return chaincodeManager;
    }


    public Collection<ProposalResponse> invokeRead(String contract, String function, String[] arguments) throws Exception {


        String channelName = Config.CHANNEL_NAME;
        int orgId = 1;
        int peerId = 0;

        String caUrl = FabricServiceProvider.CA_SERVICE.getCaUrl(orgId);

        UserContext adminUserContext = FabricServiceProvider.USER_SERVICE.getCAUserContext(caUrl, orgId, Config.ADMIN, Config.ADMIN_PASSWORD).getUserContext();
        FabricClient context = FabricClient.builder().userContext(adminUserContext).build();
        FabricChannel fabricChannel = FabricServiceProvider.CHANNEL_SERVICE.connectToChannel(channelName, orgId, peerId, context);
        return FabricServiceProvider.CHAINCODE_SERVICE.invokeRead(fabricChannel, contract, function, arguments);

    }

    public Collection<ProposalResponse> invokeWrite(String contract, String function, String[] arguments) throws Exception {
        //long totalSize = Runtime.getRuntime().totalMemory()/ (8*1024*1024);
        //long maxSize = Runtime.getRuntime().maxMemory()/ (8*1024*1024);
        //long freeSize = Runtime.getRuntime().freeMemory()/ (8*1024*1024);
        String channelName = Config.CHANNEL_NAME;
        int orgId = 1;
        int peerId = 0;
        String caUrl = FabricServiceProvider.CA_SERVICE.getCaUrl(orgId);
        //System.out.println("P1: "+ Integer.toString((int) heapSize));
        UserContext adminUserContext = FabricServiceProvider.USER_SERVICE.getCAUserContext(caUrl, orgId, Config.ADMIN, Config.ADMIN_PASSWORD).getUserContext();
        //System.out.println("P2: "+ Integer.toString((int) heapSize));
        FabricClient context = FabricClient.builder().userContext(adminUserContext).build();
        //System.out.println("P3: "+ Integer.toString((int) heapSize));
        FabricChannel fabricChannel = FabricServiceProvider.CHANNEL_SERVICE.connectToChannel(channelName, orgId, peerId, context);
        //System.out.println("FreeMemory: "+ Integer.toString((int) freeSize)+ "MB of (" + Integer.toString((int) totalSize)+"MB/"+Integer.toString((int) maxSize)+"MB[max])");

        return FabricServiceProvider.CHAINCODE_SERVICE.invokeWrite(fabricChannel, contract, function, arguments);

    }


    public boolean deploy(String contractName,
                          String path,
                          String chaincodeRootDir,
                          TransactionRequest.Type type,
                          String chaincodeVersion, String[] arguments) {
        try {


            UserContext org1Admin = FabricServiceProvider.USER_SERVICE.getUserContext(1, Config.ADMIN).getUserContext();
            UserContext org2Admin = FabricServiceProvider.USER_SERVICE.getUserContext(2, Config.ADMIN).getUserContext();

            FabricClient context = FabricClient.builder().userContext(org1Admin).build();

            FabricChannel fabricChannel = FabricServiceProvider.CHANNEL_SERVICE.connectToChannel(Config.CHANNEL_NAME, context);
            Channel channel = fabricChannel.getChannel();
            Orderer orderer = FabricServiceProvider.ORDERER_SERVICE.newOrderer(context);
            Peer peer0_org1 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 1, 0).getPeer();
            Peer peer1_org1 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 1, 1).getPeer();
            Peer peer0_org2 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 2, 0).getPeer();
            Peer peer1_org2 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 2, 1).getPeer();
            channel.addOrderer(orderer);
            channel.addPeer(peer0_org1);
            channel.addPeer(peer1_org1);
            channel.addPeer(peer0_org2);
            channel.addPeer(peer1_org2);
            channel.initialize();


            // Send to ORG1
            List<Peer> org1Peers = new ArrayList<Peer>();
            org1Peers.add(peer0_org1);
            org1Peers.add(peer1_org1);

            Collection<ProposalResponse> response = FabricServiceProvider.CHAINCODE_SERVICE
                    .deployChainCode(context, contractName, path,
                            chaincodeRootDir, type.toString(), chaincodeVersion, org1Peers);


            for (ProposalResponse res : response) {
                Logger.getLogger(ChaincodeManager.class.getName()).log(Level.INFO,
                        contractName + "- Chain code deployment " + res.getStatus() + " " + res.getMessage());
            }


            // Send to ORG2
            context.setUserContext(org2Admin);

            List<Peer> org2Peers = new ArrayList<Peer>();
            org2Peers.add(peer0_org2);
            org2Peers.add(peer1_org2);

            response = FabricServiceProvider.CHAINCODE_SERVICE
                    .deployChainCode(context, contractName, path,
                            chaincodeRootDir, type.toString(), chaincodeVersion, org2Peers);


            for (ProposalResponse res : response) {
                Logger.getLogger(ChaincodeManager.class.getName()).log(Level.INFO,
                        contractName + "- Chain code deployment " + res.getStatus() + " " + res.getMessage());
            }


            // Instantiate
            response = FabricServiceProvider.CHAINCODE_SERVICE
                    .instantiateChainCode(
                            fabricChannel,
                            contractName, chaincodeVersion,
                            path, type.toString(), "init", arguments, null);

            for (ProposalResponse res : response) {
                Logger.getLogger(ChaincodeManager.class.getName()).log(Level.INFO,
                        contractName + "- Chain code instantiation " + res.getStatus() + " " + res.getMessage());
            }


        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


}
