package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.model.FabricUserContext;

public interface EnrollmentService {

    String registerUser(FabricUserContext fabricAdminContext, String caUrl, String username, String organization) throws Exception;

    FabricUserContext enrollAdminUser(FabricUserContext fabricAdminContext, String caUrl, String username, String password) throws Exception;

    FabricUserContext enrollUser(FabricUserContext fabricAdminContext, String caUrl, FabricUserContext user, String secret) throws Exception;
}
