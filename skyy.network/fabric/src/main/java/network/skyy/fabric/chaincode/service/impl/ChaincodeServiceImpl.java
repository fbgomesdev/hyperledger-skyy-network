package network.skyy.fabric.chaincode.service.impl;

import network.skyy.fabric.chaincode.model.FabricChannel;
import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.service.ChaincodeService;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.ChaincodeEndorsementPolicyParseException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hyperledger.fabric.sdk.Channel.NOfEvents.createNofEvents;

public class ChaincodeServiceImpl implements ChaincodeService {

    private static final byte[] EXPECTED_EVENT_DATA = "!".getBytes(UTF_8);
    private static final String EXPECTED_EVENT_NAME = "event";


    private Logger logger = Logger.getLogger(ChaincodeService.class.getName());


    @Override
    public Collection<ProposalResponse> deployChainCode(
            FabricClient context,
            String chainCodeName, String chaincodePath, String codepath,
            String language, String version, Collection<Peer> peers)
            throws InvalidArgumentException, IOException, ProposalException {

        HFClient hfClient = context.getClient();


        InstallProposalRequest request = hfClient.newInstallProposalRequest();
        ChaincodeID.Builder chaincodeIDBuilder = ChaincodeID.newBuilder().setName(chainCodeName).setVersion(version);

        if (!"JAVA".equals(language)) {
            chaincodeIDBuilder.setPath(chaincodePath);
        }

        ChaincodeID chaincodeID = chaincodeIDBuilder.build();

        logger.log(Level.INFO,
                "Deploying chaincode " + chainCodeName + " using Fabric client " + hfClient.getUserContext().getMspId()
                        + " " + hfClient.getUserContext().getName());
        request.setChaincodeID(chaincodeID);
        request.setUserContext(hfClient.getUserContext());
        request.setChaincodeSourceLocation(new File(codepath));
        request.setChaincodeVersion(version);
        request.setChaincodeLanguage(TransactionRequest.Type.valueOf(language));

        return hfClient.sendInstallProposal(request, peers);

    }


    /**
     * Instantiate chaincode.
     */
    @Override
    public Collection<ProposalResponse> instantiateChainCode(
            FabricChannel fabricChannel,
            String chaincodeName, String version, String chaincodePath,
            String language, String functionName, String[] functionArgs, String policyPath)
            throws InvalidArgumentException, ProposalException, ChaincodeEndorsementPolicyParseException, IOException {

        Channel channel = fabricChannel.getChannel();
        FabricClient context = fabricChannel.getContext();
        HFClient instance = context.getClient();


        Logger.getLogger(ChaincodeServiceImpl.class.getName()).log(Level.INFO,
                "Instantiate proposal request " + chaincodeName + " on channel " + channel.getName()
                        + " with Fabric client " + instance.getUserContext().getMspId() + " "
                        + instance.getUserContext().getName());
        InstantiateProposalRequest instantiateProposalRequest = instance
                .newInstantiationProposalRequest();
        instantiateProposalRequest.setProposalWaitTime(18000000);
        ChaincodeID.Builder chaincodeIDBuilder = ChaincodeID.newBuilder().setName(chaincodeName).setVersion(version);
        //.setPath(chaincodePath);

        if (!"JAVA".equals(language)) {
            chaincodeIDBuilder.setPath(chaincodePath);
        }

        ChaincodeID ccid = chaincodeIDBuilder.build();
        Logger.getLogger(ChaincodeServiceImpl.class.getName()).log(Level.INFO,
                "Instantiating Chaincode ID " + chaincodeName + " on channel " + channel.getName());
        instantiateProposalRequest.setChaincodeID(ccid);
        if (language.equals(TransactionRequest.Type.GO_LANG.toString()))
            instantiateProposalRequest.setChaincodeLanguage(TransactionRequest.Type.GO_LANG);
        else
            instantiateProposalRequest.setChaincodeLanguage(TransactionRequest.Type.JAVA);

        instantiateProposalRequest.setFcn(functionName);
        instantiateProposalRequest.setArgs(functionArgs);
        Map<String, byte[]> tm = new HashMap<>();
        tm.put("HyperLedgerFabric", "InstantiateProposalRequest:JavaSDK".getBytes(UTF_8));
        tm.put("method", "InstantiateProposalRequest".getBytes(UTF_8));
        instantiateProposalRequest.setTransientMap(tm);
        instantiateProposalRequest.setProposalWaitTime(18000000);

        if (policyPath != null) {
            ChaincodeEndorsementPolicy chaincodeEndorsementPolicy = new ChaincodeEndorsementPolicy();
            chaincodeEndorsementPolicy.fromYamlFile(new File(policyPath));
            instantiateProposalRequest.setChaincodeEndorsementPolicy(chaincodeEndorsementPolicy);
        }

        Collection<ProposalResponse> responses = channel.sendInstantiationProposal(instantiateProposalRequest);
        CompletableFuture<BlockEvent.TransactionEvent> cf = channel.sendTransaction(responses);

        Logger.getLogger(ChaincodeServiceImpl.class.getName()).log(Level.INFO,
                "Chaincode " + chaincodeName + " on channel " + channel.getName() + " instantiation " + cf);
        return responses;
    }


    /**
     * Query by chaincode.
     */
    @Override
    public Collection<ProposalResponse> invokeRead(
            FabricChannel fabricChannel,
            String chaincodeName, String functionName, String[] args)
            throws InvalidArgumentException, ProposalException {

        Channel channel = fabricChannel.getChannel();
        FabricClient context = fabricChannel.getContext();
        HFClient instance = context.getClient();

        Logger.getLogger(ChaincodeServiceImpl.class.getName()).log(Level.INFO,
                "Querying " + functionName + " on channel " + channel.getName());

        QueryByChaincodeRequest request = instance.newQueryProposalRequest();
        ChaincodeID ccid = ChaincodeID.newBuilder().setName(chaincodeName).build();
        request.setChaincodeID(ccid);
        request.setFcn(functionName);
        if (args != null) {
            request.setArgs(args);
        }

        return channel.queryByChaincode(request);

    }


    @Override
    public Collection<ProposalResponse> invokeWrite(
            FabricChannel fabricChannel,
            String contract, String function, String[] arguments) throws Exception {

        Channel channel = fabricChannel.getChannel();
        FabricClient context = fabricChannel.getContext();
        HFClient hfClient = context.getClient();
        Channel.NOfEvents nOfEvents = createNofEvents();
        if (!channel.getPeers(EnumSet.of(Peer.PeerRole.EVENT_SOURCE)).isEmpty()) {
            nOfEvents.addPeers(channel.getPeers(EnumSet.of(Peer.PeerRole.EVENT_SOURCE)));
        }

        Collection<ProposalResponse> successful = new LinkedList<>();

        ChaincodeID ccid = ChaincodeID.newBuilder().setName(contract).build();
        TransactionProposalRequest request = hfClient.newTransactionProposalRequest();
        request.setChaincodeID(ccid);
        request.setFcn(function);
        request.setArgs(arguments);
        request.setProposalWaitTime(10000);

        Map<String, byte[]> tm2 = new HashMap<>();
        tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8));
        tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8));
        tm2.put("result", ":)".getBytes(UTF_8));
        tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA);
        request.setTransientMap(tm2);

        Collection<ProposalResponse> transactionPropResp = channel.sendTransactionProposal(request, channel.getPeers());
        for (ProposalResponse response : transactionPropResp) {
            if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
                successful.add(response);
            }
        }
        channel.sendTransaction(successful);

        return successful;
    }


}
