package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.model.FabricUserContext;
import org.hyperledger.fabric.sdk.exception.CryptoException;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public interface UserContextService {

    FabricUserContext getUserContext(int organization, String name) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, CryptoException;

    FabricUserContext getCAUserContext(String caUrl, int organization, String user, String password) throws Exception;
}
