package network.skyy.fabric.chaincode.service.impl;

import network.skyy.fabric.chaincode.model.FabricUserContext;
import network.skyy.fabric.chaincode.service.FabricServiceProvider;
import network.skyy.fabric.chaincode.service.UserContextService;
import network.skyy.fabric.user.UserContext;
import network.skyy.fabric.util.Util;
import network.skyy.network.FabricNetworkConfig;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.exception.CryptoException;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class UserContextServiceImpl implements UserContextService {


    @Override
    public FabricUserContext getUserContext(int organization, String name) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, CryptoException {

        String orgUsrAdminPk = FabricNetworkConfig.getOrgUsrAdminPk(organization);
        String orgUsrAdminCert = FabricNetworkConfig.getOrgUsrAdminCert(organization);

        File pkFolder1 = new File(orgUsrAdminPk);
        File[] pkFiles1 = pkFolder1.listFiles();

        File certFolder = new File(orgUsrAdminCert);
        File[] certFiles = certFolder.listFiles();
        Enrollment enrollOrgAdmin = Util.getEnrollment(orgUsrAdminPk, pkFiles1[0].getName(), orgUsrAdminCert, certFiles[0].getName());

        UserContext userContext = new UserContext();
        userContext.setEnrollment(enrollOrgAdmin);
        userContext.setMspId(FabricNetworkConfig.getOrgMSP(organization));
        userContext.setName(name);

        return FabricUserContext
                .builder()
                .userContext(userContext)
                .build();
    }


    @Override
    public FabricUserContext getCAUserContext(String caUrl, int organization, String user, String password) throws Exception {

        // Enroll Admin to Org1MSP
        UserContext adminUserContext = new UserContext();
        adminUserContext.setName(user);
        adminUserContext.setAffiliation("org" + organization);
        adminUserContext.setMspId(FabricNetworkConfig.getOrgMSP(organization));

        return FabricServiceProvider.ENROLLMENT_SERVICE.enrollAdminUser(
                FabricUserContext.builder().userContext(adminUserContext).build(),
                caUrl, user, password);

    }

}
