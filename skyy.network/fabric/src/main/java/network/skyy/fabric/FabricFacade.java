package network.skyy.fabric;

import org.hyperledger.fabric.sdk.TransactionRequest;

import java.util.Optional;

public interface FabricFacade {

    boolean createNewChannel();

    boolean deploy(String contractName, String path, String chaincodeRootDir, TransactionRequest.Type type, String chaincodeVersion, String[] arguments);

    boolean registerEnrollUser();

    Optional<byte[]> invokeWrite(String contract, String function, String[] arguments);

    Optional<byte[]> invokeRead(String contract, String function, String[] arguments);
}
