package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.model.FabricClient;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;

public interface OrdererService {
    Orderer newOrderer(FabricClient context) throws InvalidArgumentException;
}
