
package network.skyy.fabric.chaincode.manager;

import network.skyy.fabric.chaincode.service.FabricServiceProvider;
import network.skyy.network.Config;

public class ChannelManager {

    private static ChannelManager channelManager = new ChannelManager();

    public static ChannelManager instance() {
        return channelManager;
    }


    public boolean createNewChannel() {
        return FabricServiceProvider.CHANNEL_SERVICE.createNewChannel(Config.CHANNEL_NAME) != null;
    }
}
