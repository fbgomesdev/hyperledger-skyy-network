package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.service.impl.*;

public class FabricServiceProvider {

    public static UserContextService USER_SERVICE = new UserContextServiceImpl();

    public static PeerService PEER_SERVICE = new PeerServiceImpl();

    public static OrdererService ORDERER_SERVICE = new OrdererServiceImpl();

    public static ChannelService CHANNEL_SERVICE = new ChannelServiceImpl();

    public static ChaincodeService CHAINCODE_SERVICE = new ChaincodeServiceImpl();

    public static TransactionService TRANSACTION_SERVICE = new TransactionServiceImpl();

    public static EnrollmentService ENROLLMENT_SERVICE = new EnrollmentServiceImpl();

    public static CaService CA_SERVICE = new CaServiceImpl();

}
