package network.skyy.fabric;

public interface TaskHandler {
    boolean perform();
}
