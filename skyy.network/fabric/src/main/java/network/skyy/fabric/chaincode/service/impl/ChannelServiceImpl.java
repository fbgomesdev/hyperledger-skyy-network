/****************************************************** 
 *  Copyright 2018 IBM Corporation 
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  you may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at 
 *  http://www.apache.org/licenses/LICENSE-2.0 
 *  Unless required by applicable law or agreed to in writing, software 
 *  distributed under the License is distributed on an "AS IS" BASIS, 
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *  See the License for the specific language governing permissions and 
 *  limitations under the License.
 */
package network.skyy.fabric.chaincode.service.impl;

import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.model.FabricChannel;
import network.skyy.fabric.chaincode.service.FabricServiceProvider;
import network.skyy.fabric.chaincode.service.ChannelService;
import network.skyy.fabric.user.UserContext;
import network.skyy.fabric.util.Util;
import network.skyy.network.Config;
import network.skyy.network.FabricNetworkConfig;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.TransactionException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChannelServiceImpl implements ChannelService {


    private Logger logger = Logger.getLogger(ChannelServiceImpl.class.getName());


    @Override
    public FabricChannel createNewChannel(String name) {
        try {

            CryptoSuite.Factory.getCryptoSuite();
            Util.cleanUp();


            // Construct Channel

            UserContext org1Admin = FabricServiceProvider.USER_SERVICE.getUserContext(1, Config.ADMIN).getUserContext();
            FabricClient context = FabricClient.builder().userContext(org1Admin).build();


            Channel channel = createNewChannel(name, context).getChannel();

            Peer peer0_org1 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 1, 0).getPeer();
            Peer peer1_org1 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 1, 1).getPeer();


            channel.joinPeer(peer0_org1);
            channel.joinPeer(peer1_org1);

            Peer peer0_org2 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 2, 0).getPeer();
            Peer peer1_org2 = FabricServiceProvider.PEER_SERVICE.newPeer(context, 2, 1).getPeer();

            UserContext org2Admin = FabricServiceProvider.USER_SERVICE.getUserContext(2, Config.ADMIN).getUserContext();
            context.setUserContext(org2Admin);

            channel.joinPeer(peer0_org2);
            channel.joinPeer(peer1_org2);
            channel.initialize();


            logger.log(Level.INFO, "Channel created " + channel.getName());
            for (Object peer : channel.getPeers()) {
                Peer pr = (Peer) peer;
                logger.log(Level.INFO, pr.getName() + " at " + pr.getUrl());
            }


            return FabricChannel
                    .builder()
                    .channel(channel)
                    .context(context)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    private FabricChannel createNewChannel(String name, FabricClient context)
            throws IOException, InvalidArgumentException, TransactionException {

        User userContext = context.getUserContext();
        HFClient hfClient = context.getClient();

        Orderer orderer = FabricServiceProvider.ORDERER_SERVICE.newOrderer(context);
        String channelConfigurationPath = FabricNetworkConfig.getChannelConfigurationPath();
        ChannelConfiguration channelConfiguration = new ChannelConfiguration(new File(channelConfigurationPath));
        byte[] channelConfigurationSignatures = hfClient.getChannelConfigurationSignature(channelConfiguration, userContext);


        Channel channel = hfClient.newChannel(name, orderer, channelConfiguration, channelConfigurationSignatures);

        return FabricChannel
                .builder()
                .channel(channel)
                .context(context)
                .build();
    }

    @Override
    public FabricChannel connectToChannel(String name, FabricClient context) throws InvalidArgumentException {
        HFClient hfClient = context.getClient();
        Channel channel = hfClient.newChannel(name);

        return FabricChannel
                .builder()
                .channel(channel)
                .context(context)
                .build();
    }

    @Override
    public FabricChannel connectToChannel(String name, int orgId, int peerId, FabricClient context) throws Exception {

        FabricChannel fabricChannel = connectToChannel(name, context);

        Channel channel = fabricChannel.getChannel();

        Peer peer = FabricServiceProvider.PEER_SERVICE.newPeer(context, orgId, peerId).getPeer();
        Orderer orderer = FabricServiceProvider.ORDERER_SERVICE.newOrderer(context);
        channel.addPeer(peer);
        channel.addOrderer(orderer);
        channel.initialize();

        return fabricChannel;
    }


}
