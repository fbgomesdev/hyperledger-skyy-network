package network.skyy.fabric.chaincode.model;

import lombok.Builder;
import lombok.Data;
import org.hyperledger.fabric.sdk.Peer;

@Data
@Builder
public class FabricPeer {

    private Peer peer;

    private FabricClient context;

}

