package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.model.FabricPeer;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;

public interface PeerService {
    FabricPeer newPeer(FabricClient context, int organization, int peer) throws InvalidArgumentException;

    String getPeerUrl(int organization, int peer);

    String getPeerName(int organization, int peer);
}
