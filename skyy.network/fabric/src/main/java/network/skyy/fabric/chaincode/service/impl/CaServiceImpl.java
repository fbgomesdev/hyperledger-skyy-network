package network.skyy.fabric.chaincode.service.impl;

import network.skyy.fabric.chaincode.service.CaService;
import network.skyy.network.Config;

import java.util.HashMap;
import java.util.Map;

public class CaServiceImpl implements CaService {

    private static final Map<String, String> URL_MAP = new HashMap<String, String>() {{
        put("CA_ORG1_URL", "https://" + Config.HOST + ":7054");
        put("CA_ORG2_URL", "https://" + Config.HOST + ":8054");
    }};

    @Override
    public String getCaUrl(int orgId) {
        return URL_MAP.get("CA_ORG" + orgId + "_URL");
    }
}
