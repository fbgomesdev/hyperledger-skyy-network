package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.model.FabricClient;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.TransactionInfo;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;

import java.util.Collection;

public interface TransactionService {

    Collection<ProposalResponse> sendTransactionProposal(
            String channelName,
            FabricClient context, TransactionProposalRequest request)
            throws ProposalException, InvalidArgumentException;

    TransactionInfo queryByTransactionId(
            String channelName,
            FabricClient context,
            String txnId) throws Exception;
}
