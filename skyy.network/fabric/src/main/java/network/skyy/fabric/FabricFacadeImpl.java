package network.skyy.fabric;

import network.skyy.fabric.chaincode.manager.ChaincodeManager;
import network.skyy.fabric.chaincode.manager.ChannelManager;
import network.skyy.fabric.chaincode.manager.EnrollmentManager;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.TransactionRequest;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

class FabricFacadeImpl implements FabricFacade {

    @Override
    public boolean createNewChannel() {
        return ChannelManager.instance().createNewChannel();
    }

    @Override
    public boolean deploy(String contractName, String path, String chaincodeRootDir, TransactionRequest.Type type, String chaincodeVersion, String[] arguments) {

        return ChaincodeManager.instance()
                .deploy(contractName, path, chaincodeRootDir, type, chaincodeVersion, arguments);

    }

    @Override
    public boolean registerEnrollUser() {
        return EnrollmentManager.instance().registerEnrollUser();
    }

    @Override
    public Optional<byte[]> invokeWrite(String contract, String function, String[] arguments) {
        try {

            return ChaincodeManager.instance().invokeWrite(contract, function, arguments)
                    .stream().map(this::getPayloadFromResponse).findFirst();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Optional<byte[]> invokeRead(String contract, String function, String[] arguments) {
        try {

            return ChaincodeManager.instance().invokeRead(contract, function, arguments)
                    .stream().map(this::getPayloadFromResponse).findFirst();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] getPayloadFromResponse(ProposalResponse res) {
        try {
            byte[] payload = res.getChaincodeActionResponsePayload();
            Logger.getLogger(ChaincodeManager.class.getName()).log(Level.INFO, new String(payload));
            return payload;
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
            return null;
        }
    }
}
