package network.skyy.fabric;

public class FacadeFactory {

    static FabricFacade facade = new FabricFacadeImpl();

    public static FabricFacade instance() {
        return facade;
    }
}
