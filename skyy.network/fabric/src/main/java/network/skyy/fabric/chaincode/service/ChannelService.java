package network.skyy.fabric.chaincode.service;

import network.skyy.fabric.chaincode.model.FabricClient;
import network.skyy.fabric.chaincode.model.FabricChannel;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;

public interface ChannelService {

    FabricChannel createNewChannel(String name);

    FabricChannel connectToChannel(String name, FabricClient context) throws InvalidArgumentException;

    FabricChannel connectToChannel(String name, int orgId, int peerId, FabricClient context) throws Exception;
}
