
package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "circle",
        "type",
        "minAlt"
})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlyspaceCircle {

    @JsonProperty("circle")
    private FlyspaceCircleData circle;

    @JsonProperty("type")
    private FlyspaceType type;

    @JsonProperty("metadata")
    private FlyspaceMetadata metadata;

    @JsonProperty("minAlt")
    private Integer minAlt;

}
