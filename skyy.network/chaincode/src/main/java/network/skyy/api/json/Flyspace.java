
package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "circles",
        "polygons",
        "version"
})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Flyspace {

    @JsonProperty("circles")
    private List<FlyspaceCircle> circles = null;

    @JsonProperty("polygons")
    private List<FlyspacePolygon> polygons = null;

    @JsonProperty("version")
    private Integer version = 2;

}
