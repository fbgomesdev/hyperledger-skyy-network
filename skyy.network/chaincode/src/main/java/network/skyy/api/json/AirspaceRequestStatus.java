package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum AirspaceRequestStatus {

    @JsonProperty("approved")
    APPROVED,

    @JsonProperty("pending")
    PENDING,

    @JsonProperty("rejected")
    REJECTED,

    @JsonProperty("successful")
    SUCCESSFUL,

    @JsonProperty("failed")
    FAILED,

    @JsonProperty("expired")
    EXPIRED;
}
