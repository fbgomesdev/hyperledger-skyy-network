package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ResponseStatus {

    @JsonProperty("successful")
    SUCCESSFUL,

    @JsonProperty("failed")
    FAILED
}
