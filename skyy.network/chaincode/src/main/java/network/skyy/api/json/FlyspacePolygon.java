
package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "polygon",
        "type",
        "minAlt"
})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlyspacePolygon {

    @JsonProperty("polygon")
    private List<List<Double>> polygon = null;

    @JsonProperty("type")
    private FlyspaceType type;

    @JsonProperty("metadata")
    private FlyspaceMetadata metadata;

    @JsonProperty("minAlt")
    private Integer minAlt;

}
