package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "skyyLedgerData"
})
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AirspaceRequest extends Geodata {

    @JsonProperty("id")
    private String id;

    @JsonProperty("skyyLedgerData")
    private SkyyLedgerData skyyLedgerData;

    @JsonProperty("requestStatus")
    private AirspaceRequestStatus status = AirspaceRequestStatus.PENDING;

}
