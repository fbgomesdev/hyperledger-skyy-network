
package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fileType",
        "flyspace",
        "groundStation",
        "version"
})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Geodata {

    @JsonProperty("fileType")
    protected String fileType = "Plan";

    @JsonProperty("flyspace")
    protected Flyspace flyspace;

    @JsonProperty("groundStation")
    protected String groundStation = "QGroundControl";

    @JsonProperty("version")
    protected Integer version = 1;


}
