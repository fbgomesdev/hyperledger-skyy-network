
package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
})
@Builder
@Data
public class FlyspaceMetadata {

    @JsonProperty("name")
    private String name;

    @JsonProperty("skyyLedgerData")
    private SkyyLedgerData skyyLedgerData;

}
