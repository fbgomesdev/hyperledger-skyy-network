package network.skyy.chaincode.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.AirspaceRequest;
import network.skyy.chaincode.handlers.factory.MethodHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class RegisterAirspaceRequestHandler implements MethodHandler {

    private static Log logger = LogFactory.getLog(RegisterAirspaceRequestHandler.class);


    @Override
    public byte[] execute(ChaincodeStub stub, List<String> args) throws IOException {
        logger.info("Registering airspace request");

        AirspaceRequest request = new ObjectMapper().readValue(args.get(0), AirspaceRequest.class);
        String objectKey = UUID.randomUUID().toString();
        request.setId(objectKey);
        byte[] value = new ObjectMapper().writeValueAsBytes(request);

        stub.putState(objectKey, value);

        return objectKey.getBytes();
    }
}
