package network.skyy.chaincode.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ResponseUtil {

    private static Log logger = LogFactory.getLog(ResponseUtil.class);

    public static <T> List<T> getResult(QueryResultsIterator<KeyValue> queryResult, Class<T> resultClass) {
        List<T> result = new ArrayList<>();
        queryResult.forEach(request -> {
            try {
                logger.info(request.getStringValue());
                result.add(new ObjectMapper().readValue(request.getValue(), resultClass));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        return result;
    }


}
