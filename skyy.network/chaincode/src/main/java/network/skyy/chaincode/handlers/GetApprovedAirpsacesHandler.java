package network.skyy.chaincode.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import network.skyy.api.json.AirspaceRequest;
import network.skyy.api.json.AirspaceRequestStatus;
import network.skyy.api.json.JSONDuration;
import network.skyy.chaincode.handlers.factory.MethodHandler;
import network.skyy.chaincode.util.ResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;
import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

public class GetApprovedAirpsacesHandler implements MethodHandler {

    private static Log logger = LogFactory.getLog(GetAirspaceToBeApprovedHandler.class);

    @Override
    public byte[] execute(ChaincodeStub stub, List<String> args) throws JsonProcessingException {
        JsonObject query = new JsonObject();
        JsonObject selector = new JsonObject();
        selector.addProperty("requestStatus", "approved");
        query.add("selector", selector);

        QueryResultsIterator<KeyValue> approvedRequests = stub.getQueryResult(query.toString());
        List<AirspaceRequest> result = ResponseUtil.getResult(approvedRequests, AirspaceRequest.class);

        logger.info("Result: " + result);

        List<AirspaceRequest> expiredAirspaces = result.stream().filter(a -> {
            Calendar approvedTimeLimit = getApprovedTimeLimit(a);
            return approvedTimeLimit.getTimeInMillis() < System.currentTimeMillis();
        }).collect(Collectors.toList());

        logger.info("Expired: " + expiredAirspaces);

        for(AirspaceRequest expiredAirspace : expiredAirspaces) {
            result.removeIf(a -> a.getId().equals(expiredAirspace.getId()));
            expiredAirspace.setStatus(AirspaceRequestStatus.EXPIRED);
            stub.putState(expiredAirspace.getId(), new ObjectMapper().writeValueAsBytes(expiredAirspace));
        }

        result.stream().forEach(a -> {
            JSONDuration duration = a.getSkyyLedgerData().getDuration();
            Calendar approvedTimeLimit = getApprovedTimeLimit(a);

            long approvedTimeInMinutes = approvedTimeLimit.getTimeInMillis() / 60000;
            long currentTimeInMinutes = System.currentTimeMillis() / 60000;
            duration.setRemainingMinutes((int) (approvedTimeInMinutes - currentTimeInMinutes));
        });

        return new ObjectMapper().writeValueAsBytes(result);
    }

    @NotNull
    private Calendar getApprovedTimeLimit(AirspaceRequest a) {
        JSONDuration duration = a.getSkyyLedgerData().getDuration();
        Timestamp approvedTimestamp = duration.getApprovedTimestamp();

        Calendar approvedTimeLimit = Calendar.getInstance();
        approvedTimeLimit.setTimeInMillis(approvedTimestamp.getTime());
        approvedTimeLimit.add(Calendar.MINUTE, duration.getCount());
        return approvedTimeLimit;
    }

}
