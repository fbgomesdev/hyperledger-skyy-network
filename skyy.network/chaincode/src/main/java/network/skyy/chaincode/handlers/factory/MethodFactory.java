package network.skyy.chaincode.handlers.factory;

public class MethodFactory {

    public static MethodHandler getMethodHandler(String type) {
        try {
            return MethodType.fromName(type).getHandlerInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
