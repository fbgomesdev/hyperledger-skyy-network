package network.skyy.chaincode;

import com.google.protobuf.ByteString;
import io.netty.handler.ssl.OpenSsl;
import network.skyy.chaincode.handlers.factory.MethodFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeBase;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public class MainController extends ChaincodeBase {

    private static Log logger = LogFactory.getLog(MainController.class);

    @Override
    public Response init(ChaincodeStub stub) {
        logger.info("init");
        return newSuccessResponse("init", ByteString.copyFrom("Init", UTF_8).toByteArray());
    }

    @Override
    public Response invoke(ChaincodeStub stub) {
        try {
            List<String> params = stub.getParameters();
            logger.info("invoke" + stub.getFunction());

            byte[] response = MethodFactory.getMethodHandler(stub.getFunction()).execute(stub, params);

            return newSuccessResponse("Response:", response);
        } catch (Throwable e) {
            e.printStackTrace();
            return newErrorResponse(e.getMessage());
        }
    }

    public static void main(String[] args) {
        System.out.println("OpenSSL avaliable: " + OpenSsl.isAvailable());
        new MainController().start(args);
    }

}
