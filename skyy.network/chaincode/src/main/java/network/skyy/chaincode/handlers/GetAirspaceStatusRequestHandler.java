package network.skyy.chaincode.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.AirspaceRequest;
import network.skyy.chaincode.handlers.factory.MethodHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.util.List;

public class GetAirspaceStatusRequestHandler implements MethodHandler {

    private static Log logger = LogFactory.getLog(GetAirspaceStatusRequestHandler.class);


    @Override
    public byte[] execute(ChaincodeStub stub, List<String> args) throws IOException {
        logger.info("Getting airspace status");

        String airspaceRequestUid = args.get(0);
        AirspaceRequest airspaceRequest = new ObjectMapper().readValue(stub.getState(airspaceRequestUid), AirspaceRequest.class);

        return new ObjectMapper().writeValueAsBytes(airspaceRequest.getStatus());
    }
}
