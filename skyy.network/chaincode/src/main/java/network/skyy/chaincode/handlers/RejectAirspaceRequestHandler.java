package network.skyy.chaincode.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.AirspaceRequest;
import network.skyy.api.json.AirspaceRequestStatus;
import network.skyy.chaincode.handlers.factory.MethodHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.util.List;

public class RejectAirspaceRequestHandler implements MethodHandler {

    private static Log logger = LogFactory.getLog(RejectAirspaceRequestHandler.class);


    @Override
    public byte[] execute(ChaincodeStub stub, List<String> args) throws IOException {
        logger.info("Rejecting airspace request");

        String airspaceRequestUid = args.get(0);
        AirspaceRequest airspaceRequest = new ObjectMapper().readValue(stub.getState(airspaceRequestUid), AirspaceRequest.class);

        if (!airspaceRequest.getStatus().equals(AirspaceRequestStatus.PENDING)) {
            logger.info(
                    String.format(
                            "Could not reject airspace request. Airspace request is already %s",
                            airspaceRequest.getStatus().toString())
            );
            return "".getBytes();
        }

        airspaceRequest.setStatus(AirspaceRequestStatus.REJECTED);

        stub.putState(airspaceRequestUid, new ObjectMapper().writeValueAsBytes(airspaceRequest));

        return "".getBytes();
    }
}
