package network.skyy.chaincode.handlers.factory;

import network.skyy.chaincode.handlers.*;

import java.util.Arrays;
import java.util.Optional;

public enum MethodType {
    REGISTER_AIRSPACE_REQUEST(RegisterAirspaceRequestHandler.class),
    GET_AIRSPACE_TO_APPROVE(GetAirspaceToBeApprovedHandler.class),
    GET_APPROVED_AIRPACES(GetApprovedAirpsacesHandler.class),
    APPROVE_AIRSPACE(ApproveAirspaceRequestHandler.class),
    REJECT_AIRSPACE(RejectAirspaceRequestHandler.class),
    GET_AIRSPACE_STATUS(GetAirspaceStatusRequestHandler.class);

    private Class<?> handler;

    MethodType(Class<?> handler) {
        this.handler = handler;
    }

    public static MethodType fromName(String name) {
        Optional<MethodType> type = Arrays.stream(MethodType.values())
                .filter(t -> t.name().equalsIgnoreCase(name))
                .findFirst();

        if (!type.isPresent()) {
            throw new RuntimeException("Method type missing: " + name);
        }

        return type.get();
    }

    public MethodHandler getHandlerInstance() throws IllegalAccessException, InstantiationException {
        Object instance = this.handler.newInstance();
        return instance != null ? (MethodHandler) instance : null;
    }

}
