package network.skyy.chaincode.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.AirspaceRequest;
import network.skyy.api.json.AirspaceRequestStatus;
import network.skyy.api.json.FlyspaceType;
import network.skyy.chaincode.handlers.factory.MethodHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

public class ApproveAirspaceRequestHandler implements MethodHandler {

    private static Log logger = LogFactory.getLog(ApproveAirspaceRequestHandler.class);


    @Override
    public byte[] execute(ChaincodeStub stub, List<String> args) throws IOException {
        logger.info("Approving airspace request");

        String airspaceRequestUid = args.get(0);
        AirspaceRequest airspaceRequest = new ObjectMapper().readValue(stub.getState(airspaceRequestUid), AirspaceRequest.class);

        if (!airspaceRequest.getStatus().equals(AirspaceRequestStatus.PENDING)) {
            logger.info(
                    String.format(
                            "Could not approve airspace request. Airspace request is already %s",
                            airspaceRequest.getStatus().toString())
            );
            return "".getBytes();
        }

        airspaceRequest.setStatus(AirspaceRequestStatus.APPROVED);

        airspaceRequest.getFlyspace().getCircles().stream().forEach(c -> c.setType(FlyspaceType.SKYY_LEDGER));
        airspaceRequest.getFlyspace().getPolygons().stream().forEach(c -> c.setType(FlyspaceType.SKYY_LEDGER));
        airspaceRequest.getSkyyLedgerData().getDuration().setApprovedTimestamp(new Timestamp(System.currentTimeMillis()));

        stub.putState(airspaceRequestUid, new ObjectMapper().writeValueAsBytes(airspaceRequest));

        return "".getBytes();
    }
}
