package network.skyy.chaincode.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import network.skyy.api.json.AirspaceRequest;
import network.skyy.chaincode.handlers.factory.MethodHandler;
import network.skyy.chaincode.util.ResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import java.util.List;

public class GetAirspaceToBeApprovedHandler implements MethodHandler {

    private static Log logger = LogFactory.getLog(GetAirspaceToBeApprovedHandler.class);

    @Override
    public byte[] execute(ChaincodeStub stub, List<String> args) throws JsonProcessingException {
        JsonObject query = new JsonObject();
        JsonObject selector = new JsonObject();
        selector.addProperty("requestStatus", "pending");
        query.add("selector", selector);

        logger.info("Query:" + query.toString());
        QueryResultsIterator<KeyValue> pendingRequests = stub.getQueryResult(query.toString());
        logger.info("Result: " + pendingRequests);

        List<AirspaceRequest> result = ResponseUtil.getResult(pendingRequests, AirspaceRequest.class);
        return new ObjectMapper().writeValueAsBytes(result);
    }

}
