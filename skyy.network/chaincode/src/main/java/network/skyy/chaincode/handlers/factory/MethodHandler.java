package network.skyy.chaincode.handlers.factory;

import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.util.List;

public interface MethodHandler {

    byte[] execute(ChaincodeStub stub, List<String> args) throws IOException;

}
