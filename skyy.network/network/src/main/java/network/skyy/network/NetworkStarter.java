package network.skyy.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class NetworkStarter {

    public static void main(String args[]) throws IOException, InterruptedException {



        //String command = NetworkStarter.class.getClassLoader().getResource().getPath();


        execute("docker-compose -f docker-compose.yml down");
        execute("docker-compose -f docker-compose.yml up");

//
//        String command = NetworkStarter.class.getClassLoader().getResource("build.sh").getPath();
//        Process process = Runtime.getRuntime().exec(new String[]{command});
//
//
////
//
//        ProcessBuilder processBuilder = new ProcessBuilder("bash", command);
////        Process process = processBuilder.start();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//        String line;
//        while ((line = reader.readLine()) != null) {
//            System.out.println(line);
//        }
////
//        int exitVal = process.waitFor();
//        if (exitVal == 0) {
//            System.out.println("Success!");
//            System.exit(0);
//        }
        //else {
//            //abnormal...
//            System.exit(exitVal);
//        }
    }

    private static void execute(String command) throws IOException, InterruptedException {
        boolean isWindows = System.getProperty("os.name")
                .toLowerCase().startsWith("windows");
        Process process;
        if (isWindows) {
            process = Runtime.getRuntime()
                    .exec(String.format("cmd.exe /c %s", command));
        } else {
            process = Runtime.getRuntime()
                    .exec(String.format("sh -c %s", command));
        }
        StreamGobbler streamGobbler =
                new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        assert exitCode == 0;
    }

    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines()
                    .forEach(consumer);
        }
    }
}
