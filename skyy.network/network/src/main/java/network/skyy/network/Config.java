package network.skyy.network;

public class Config {

    public static final String ADMIN = "admin";
    public static final String ADMIN_PASSWORD = "adminpw";

    public static final String HOST = "localhost";
    public static final String DOMAIN = "skyy.network";

    public static final String CHANNEL_NAME = "skyychannel";


}
