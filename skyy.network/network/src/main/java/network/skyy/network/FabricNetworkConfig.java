package network.skyy.network;

import java.io.File;


public class FabricNetworkConfig {


    public static String getChannelConfigurationPath() {
        final String CHANNEL_CONFIG_PATH = "network-config/config/channel.tx";

        return getFilePath(CHANNEL_CONFIG_PATH);
    }

    public static String getOrdererCertPath() {

        final String ORDERER_CERT_PATH = "network-config/crypto-config/ordererOrganizations/" + Config.DOMAIN + "/orderers/orderer." + Config.DOMAIN + "/tls/server.crt";

        return getFilePath(ORDERER_CERT_PATH);
    }

    public static String getPeerCertPath(int organization, int peer) {
        final String PEER_TLS_CERT_PATH_FORMAT =
                "network-config/crypto-config/peerOrganizations/org%d." + Config.DOMAIN + "/peers/peer%d.org%d." + Config.DOMAIN + "/tls/server.crt";

        String format = String.format(PEER_TLS_CERT_PATH_FORMAT, organization, peer, organization);
        return getFilePath(format);
    }

    public static String getCACertPath(int organization) {
        final String CA_TLS_CERT_PATH_FORMAT =
                "network-config/crypto-config/peerOrganizations/org%d." + Config.DOMAIN + "/ca/ca.org%d." + Config.DOMAIN + "-cert.pem";

        String format = String.format(CA_TLS_CERT_PATH_FORMAT, organization, organization);
        return getFilePath(format);

    }

    /**
     * @param index
     * @return
     */
    public static String getOrgUsrAdminPk(int index) {
        String ORG1_USR_BASE_PATH = getOrgUsrBasePath(index);
        return ORG1_USR_BASE_PATH + "/keystore";
    }

    /**
     * @param index
     * @return
     */
    public static String getOrgUsrAdminCert(int index) {
        String ORG1_USR_BASE_PATH = getOrgUsrBasePath(index);
        return ORG1_USR_BASE_PATH + File.separator + "admincerts";
    }

    private static String getOrgUsrBasePath(int index) {
        String orgIdent = "org" + index + "." + Config.DOMAIN;
        String name = "network-config/crypto-config/peerOrganizations/" + orgIdent + "/users/Admin@" + orgIdent + "/msp";
        return getFilePath(name);
    }

    private static String getFilePath(String name) {
        boolean isExploded = "exploded".equals(System.getProperty("mode"));
        if (isExploded) {
            System.out.println(new File("").getAbsolutePath() + "/" + name);
            return new File("").getAbsolutePath() + "/" + name;
        }

        return FabricNetworkConfig.class.getClassLoader().getResource(name).getPath();
    }


    public static String getOrgMSP(int index) {
        return "Org" + index + "MSP";
    }

    public static String getOrgName(int index) {
        return "org" + index;
    }


}
