#
#    SPDX-License-Identifier: Apache-2.0
#

#!/bin/bash
#
#Redirecting console.log to log file.
#Please visit ./logs/app to view the application logs and visit the ./logs/db to view the Database logs and visit the ./log/console for the console.log
# Log rotating for every 7 days.

rm -rf /tmp/fabric-client-kvs_peerOrg*

mkdir -p /opt/explorer/logs/app & mkdir -p /opt/explorer/logs/db & mkdir -p /opt/explorer/logs/console

LOG_CONSOLE_PATH="/opt/explorer/logs/console/console-$(date +%Y-%m-%d).log"

echo "************************************************************************************"
echo "**************************** Hyperledger Explorer **********************************"
echo "************************************************************************************"
echo "***** Please check the log [$LOG_CONSOLE_PATH] for any error *****"
echo "************************************************************************************"

node /opt/explorer/main.js >>$LOG_CONSOLE_PATH 2>&1 &

find /opt/explorer/logs/app -mtime +7 -type f -delete & find /opt/explorer/logs/db -mtime +7 -type f -delete & find /opt/explorer/logs/console -mtime +7 -type f -delete



