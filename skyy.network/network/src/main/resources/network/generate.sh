#!/bin/bash
#
# Exit on first error, print all commands.
set -e

#Start from here
echo -e "\nRegenerate artifacts"

 ./cryptogen generate --config=./crypto-config.yaml
 mkdir config
 ./configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./config/genesis.block
 ./configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./config/channel.tx -channelID skyychannel
 ./configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./config/Org1MSPanchors.tx -channelID skyychannel -asOrg Org1MSP
 ./configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./config/Org2MSPanchors.tx -channelID skyychannel -asOrg Org2MSP


