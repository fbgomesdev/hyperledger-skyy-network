package network.skyy.api.chaincode.response;

import org.apache.logging.log4j.util.Strings;

public class EmptyResponseTransformer implements ResponseTransformer<String> {
    @Override
    public String transform(byte[] source) {
        return Strings.EMPTY;
    }
}
