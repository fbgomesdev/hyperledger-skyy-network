package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum FlyspaceType {

    @JsonProperty("no-fly")
    NO_FLY,
    @JsonProperty("skyy-ledger")
    SKYY_LEDGER,

    @JsonProperty("Prohibited")
    Prohibited,
    @JsonProperty("Restricted")
    Restricted,
    @JsonProperty("Danger")
    Danger,

    @JsonProperty("CTA")
    CTA,
    @JsonProperty("Warning")
    Warning,
    @JsonProperty("OtherSUAS")
    OtherSUAS,
    @JsonProperty("MOA")
    MOA,
    @JsonProperty("ADIZ")
    ADIZ,
    @JsonProperty("OCA")
    OCA,
    @JsonProperty("FuelDumpingArea")
    FuelDumpingArea,
    @JsonProperty("CTR")
    CTR,
    @JsonProperty("Alert")
    Alert,
    @JsonProperty("TsaTraCba")
    TsaTraCba,
    @JsonProperty("TMA")
    TMA,

    @JsonProperty("pending")
    PENDING,;
}
