
package network.skyy.api.imports;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airport"
})
@XmlRootElement(name = "AIRPORTS")
public class XML_Airports {

    @XmlElement(name = "AIRPORT", required = true)
    protected List<XML_AIRPORT> airport;

    public List<XML_AIRPORT> getAIRPORT() {
        if (airport == null) {
            airport = new ArrayList<XML_AIRPORT>();
        }
        return this.airport;
    }



}
