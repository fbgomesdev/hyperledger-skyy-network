package network.skyy.api.model.airport;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode
public class TowerFrequency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private double frequency;

    @ManyToOne
    @JoinColumn(name = "airport_id")
    private Airport airport;

    public TowerFrequency(double frequency, Airport airport) {
        this.frequency = frequency;
        this.airport = airport;
    }

}
