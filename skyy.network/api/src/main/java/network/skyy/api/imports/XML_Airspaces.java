
package network.skyy.api.imports;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;
import java.util.List;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "airspace"
})
@Getter
@Setter
@XmlRootElement(name = "Airspaces")
public class XML_Airspaces {

    @XmlElement(name = "Airspace", required = true)
    protected List<XML_Airspaces.Airspace> airspace;


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "opsHours",
            "polygon",
            "verticalLimit"
    })
    @Getter @Setter
    public static class Airspace {

        @XmlElement(name = "OpsHours")
        protected String opsHours;
        @XmlElement(name = "Polygon", required = true)
        protected String polygon;
        @XmlElement(name = "VerticalLimit", required = true)
        protected List<XML_Airspaces.Airspace.VerticalLimit> verticalLimit;
        @XmlAttribute(name = "AirspaceDbId")
        protected BigInteger airspaceDbId;
        @XmlAttribute(name = "Name", required = true)
        protected String name;
        @XmlAttribute(name = "Designator", required = true)
        protected String designator;
        @XmlAttribute(name = "Type", required = true)
        protected String type;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;
        @XmlAttribute(name = "CountryName")
        protected String countryName;


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "lowerLimit",
                "upperLimit"
        })
        @Getter @Setter
        public static class VerticalLimit {

            @XmlElement(name = "LowerLimit", required = true)
            protected XML_Airspaces.Airspace.VerticalLimit.LowerLimit lowerLimit;
            @XmlElement(name = "UpperLimit", required = true)
            protected XML_Airspaces.Airspace.VerticalLimit.UpperLimit upperLimit;
            @XmlAttribute(name = "Class")
            protected String clazz;


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "value"
            })
            @Getter
            @Setter
            public static class LowerLimit {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "Unit")
                protected String unit;
                @XmlAttribute(name = "Altimeter")
                protected String altimeter;

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "value"
            })
            @Getter
            @Setter
            public static class UpperLimit {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "Unit")
                protected String unit;
                @XmlAttribute(name = "Altimeter")
                protected String altimeter;

            }

        }

    }

}
