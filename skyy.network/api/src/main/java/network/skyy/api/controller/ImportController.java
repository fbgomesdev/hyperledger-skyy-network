package network.skyy.api.controller;

import network.skyy.api.model.airport.Airport;
import network.skyy.api.model.airspace.Airspace;
import network.skyy.api.service.AirportService;
import network.skyy.api.service.AirspaceService;
import network.skyy.api.imports.SkyDataXmlImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ImportController {

    @Autowired
    AirspaceService airspaceService;

    @Autowired
    AirportService airportService;


    @RequestMapping(path = "import", method = RequestMethod.GET)
    public boolean importSkyData() throws Exception {

        SkyDataXmlImporter xmlImporter = new SkyDataXmlImporter();

        List<Airspace> airspaces = xmlImporter.importAirspaces();
        airspaceService.saveAll(airspaces);

        List<Airport> airports = xmlImporter.importAirports();
        airportService.saveAll(airports);


        return true;
    }

}
