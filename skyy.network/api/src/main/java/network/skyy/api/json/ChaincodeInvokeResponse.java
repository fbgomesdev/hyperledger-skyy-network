package network.skyy.api.json;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChaincodeInvokeResponse {

    private ResponseStatus status;

    private String message;

    private Object payload;
}
