package network.skyy.api.chaincode;

import lombok.Getter;

public enum Chaincodes {
    SKYY_LEDGER("skyyledger");

    @Getter
    private String name;

    Chaincodes(String name) {
        this.name = name;
    }
}
