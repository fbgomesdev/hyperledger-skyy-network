package network.skyy.api.controller;

import network.skyy.api.json.Geodata;
import network.skyy.api.model.airport.Airport;
import network.skyy.api.model.airspace.Airspace;
import network.skyy.api.service.AirportService;
import network.skyy.api.service.AirspaceService;
import network.skyy.api.service.ChaincodeService;
import network.skyy.api.service.GeodataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "airspace")
public class AirspaceController {

    int maxRange = 150;

    @Autowired
    AirspaceService airspaceService;

    @Autowired
    AirportService airportService;

    @Autowired
    GeodataService geodataService;

    @Autowired
    ChaincodeService chaincodeService;

    @RequestMapping(value = "inrange", method = RequestMethod.GET)
    public Geodata getGeodata(double longitude,
                              double latitude,
                              int range,
                              @RequestParam(defaultValue = "5000") int minAltitudeLowerThan) {

        if (range < 0) {
            throw new IndexOutOfBoundsException("Range must be greater than 0");
        }

        if (range > maxRange) {
            range = maxRange;
        }

        List<Airspace> airspaces = airspaceService.findAirspacesInRange(longitude, latitude, range,minAltitudeLowerThan);
        List<Airport> airports = airportService.findAirportsInRange(longitude, latitude, range);
        return geodataService.create(airspaces,airports);
    }

}
