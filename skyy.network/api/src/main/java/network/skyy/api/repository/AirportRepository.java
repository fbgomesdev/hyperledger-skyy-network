package network.skyy.api.repository;

import network.skyy.api.model.airport.Airport;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportRepository extends CrudRepository<Airport, Integer> {

    @Query(value = "SELECT * FROM Airport arp WHERE " +
            "st_dwithin(cast(arp.coordinate as geography), cast( ST_MakePoint(:longitude,:latitude) as geography),:range*1000,true)",
            nativeQuery = true)
    List<Airport> findAirportsInRange(@Param("longitude") double longitude,
                                      @Param("latitude") double latitude,
                                      @Param("range") int range);
}
