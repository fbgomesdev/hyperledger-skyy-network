package network.skyy.api.service;

import network.skyy.api.repository.AirspaceRepository;
import network.skyy.api.model.airspace.Airspace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirspaceService {

    @Autowired
    private AirspaceRepository airspaceRepository;

    public void save(Airspace airspace) {
        airspaceRepository.save(airspace);
    }

    public Airspace findById(int id) {
        Optional<Airspace> result = airspaceRepository.findById(id);
        return result.get();
    }

    public void saveAll(List<Airspace> airspaces) {
        airspaceRepository.saveAll(airspaces);
    }


    public List<Airspace> findAirspacesInRange(double longitude, double latitude, int range, int minAltitudeLowerThan){
        return airspaceRepository.findAirspacesInRange(longitude,latitude,range,minAltitudeLowerThan);

    }

}
