package network.skyy.api.model.airspace;


import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Polygon;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@EqualsAndHashCode
public class Airspace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String airspaceName;

    @NotNull
    private String designator;

    @NotNull
    private String airspaceType;

    private String countryCode;

    private String countryName;

    private String operatingHours;

    @JsonSerialize(using = GeometrySerializer.class)
    private Polygon polygon;

    @OneToMany(mappedBy = "airspace", cascade = CascadeType.ALL)
    List<VerticalLimit> verticalLimits = new ArrayList<>();

}
