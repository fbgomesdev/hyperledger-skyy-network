package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "max",
        "min",
        "unit"
})
@NoArgsConstructor
@AllArgsConstructor
@Data
public class JSONAltitude {

    @JsonProperty("max")
    private int max;

    @JsonProperty("min")
    private int min;

    @JsonProperty("unit")
    private String unit;

}