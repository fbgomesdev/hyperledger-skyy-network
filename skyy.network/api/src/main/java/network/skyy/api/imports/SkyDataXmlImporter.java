package network.skyy.api.imports;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import network.skyy.api.model.airport.*;
import network.skyy.api.model.airspace.Airspace;
import network.skyy.api.model.airspace.AltitudeLimit;
import network.skyy.api.model.airspace.VerticalLimit;
import org.springframework.core.io.ClassPathResource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SkyDataXmlImporter {

    public List<Airspace> importAirspaces() throws Exception {

        List<Airspace> airspaces = new ArrayList<>();

        try {
            InputStream file = getXmlResource("airspaces");

            JAXBContext jaxbContext = JAXBContext.newInstance(network.skyy.api.imports.XML_Airspaces.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            network.skyy.api.imports.XML_Airspaces xml_airspaces = (network.skyy.api.imports.XML_Airspaces) jaxbUnmarshaller.unmarshal(file);
            for (network.skyy.api.imports.XML_Airspaces.Airspace xml_airspace : xml_airspaces.airspace) {
                Airspace airspace = convertToAirspace(xml_airspace);
                airspaces.add(airspace);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return airspaces;
    }

    private Airspace convertToAirspace(network.skyy.api.imports.XML_Airspaces.Airspace xml_airspace) {
        Airspace airspace = new Airspace();

        airspace.setOperatingHours(xml_airspace.opsHours);
        airspace.setPolygon(generatePolygon(xml_airspace.polygon));
        airspace.setAirspaceName(xml_airspace.name);
        airspace.setDesignator(xml_airspace.designator);
        airspace.setAirspaceType(xml_airspace.type);
        airspace.setCountryCode(xml_airspace.countryCode);
        airspace.setCountryName(xml_airspace.countryName);

        for (network.skyy.api.imports.XML_Airspaces.Airspace.VerticalLimit xml_verticalLimit : xml_airspace.verticalLimit) {
            AltitudeLimit lowerLimit = new AltitudeLimit();
            AltitudeLimit upperLimit = new AltitudeLimit();

            lowerLimit.setAltimeter(xml_verticalLimit.lowerLimit.altimeter);
            lowerLimit.setUnit(xml_verticalLimit.lowerLimit.unit);

            upperLimit.setAltimeter(xml_verticalLimit.upperLimit.altimeter);
            upperLimit.setUnit(xml_verticalLimit.upperLimit.unit);

            if (xml_verticalLimit.lowerLimit.value.toLowerCase().contains("ground")) {
                lowerLimit.setValue(0);
                lowerLimit.setGround(true);
            } else {
                double value = Double.parseDouble(xml_verticalLimit.lowerLimit.value);
                lowerLimit.setValue(value);
            }

            if (xml_verticalLimit.upperLimit.value.toLowerCase().contains("unlimited")) {
                upperLimit.setValue(0);
                upperLimit.setUnlimited(true);
            } else {
                double value = Double.parseDouble(xml_verticalLimit.upperLimit.value);
                upperLimit.setValue(value);
            }

            VerticalLimit verticalLimit = new VerticalLimit();

            verticalLimit.setAirspace(airspace);
            verticalLimit.setClazz(xml_verticalLimit.clazz);
            verticalLimit.setLowerLimit(lowerLimit);
            verticalLimit.setUpperLimit(upperLimit);

            airspace.getVerticalLimits().add(verticalLimit);
        }

        return airspace;
    }

    private Polygon generatePolygon(String xml_polygon) {
        String[] xml_points = xml_polygon.split(" ");
        Coordinate[] points = new Coordinate[xml_points.length / 2 + 1];

        for (int i = 0; i < xml_points.length; i = i + 2) {
            double longitude = Double.parseDouble(xml_points[i]);
            double latitude = Double.parseDouble(xml_points[i + 1]);
            points[i / 2] = new Coordinate(longitude, latitude);
        }
        double longitude = Double.parseDouble(xml_points[0]);
        double latitude = Double.parseDouble(xml_points[1]);
        points[points.length - 1] = new Coordinate(longitude, latitude);
        return new GeometryFactory().createPolygon(points);
    }

    public List<Airport> importAirports() throws Exception {
        List<Airport> airports = new ArrayList<>();

        try {
            InputStream file = getXmlResource("airports");

            JAXBContext jaxbContext = JAXBContext.newInstance(network.skyy.api.imports.XML_Airports.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            network.skyy.api.imports.XML_Airports xml_airports = (network.skyy.api.imports.XML_Airports) jaxbUnmarshaller.unmarshal(file);
            for (network.skyy.api.imports.XML_AIRPORT xml_airport : xml_airports.airport) {
                Airport airport = convertToAirport(xml_airport);
                airports.add(airport);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return airports;
    }

    private Airport convertToAirport(network.skyy.api.imports.XML_AIRPORT xml_airport) {

        Airport airport = new Airport();

        double longitude = Double.parseDouble(xml_airport.aptRefPointDD.longitudeDD);
        double latitude = Double.parseDouble(xml_airport.aptRefPointDD.latitudeDD);
        Point location = new GeometryFactory().createPoint(new Coordinate(longitude, latitude));

        AptRefPoint_DD aptRefPoint_dd = new AptRefPoint_DD();
        aptRefPoint_dd.setLatitudeDD(Double.parseDouble(xml_airport.aptRefPointDD.latitudeDD));
        aptRefPoint_dd.setLongitudeDD(Double.parseDouble(xml_airport.aptRefPointDD.longitudeDD));

        AptRefPoint aptRefPoint = new AptRefPoint();
        aptRefPoint.setLatitudeDD(xml_airport.aptRefPoint.latitude);
        aptRefPoint.setLongitudeDD(xml_airport.aptRefPoint.longitude);

        Authority authority = new Authority();
        authority.setAftn(xml_airport.authorities.aftn);
        authority.setSitaTelex(xml_airport.authorities.sitaTelex);

        if (xml_airport.authorities.operator.operatorLine != null) {
            for (String line : xml_airport.authorities.operator.operatorLine) {
                authority.getOperator().add(new OperatorLine(line, authority));
            }
        }
        if (xml_airport.authorities.telephones.telephone != null) {
            for (String number : xml_airport.authorities.telephones.telephone) {
                authority.getTelephones().add(new Telephone(number, authority));
            }
        }
        if (xml_airport.authorities.faxes.fax != null) {
            for (String fax : xml_airport.authorities.faxes.fax) {
                authority.getFaxes().add(new Fax(fax, authority));
            }
        }
        if (xml_airport.authorities.postAdress.addressLine != null) {
            for (String line : xml_airport.authorities.postAdress.addressLine) {
                authority.getPostAddress().add(new PostAddressLine(line, authority));
            }
        }
        if (xml_airport.authorities.emails.email != null) {
            for (String email : xml_airport.authorities.emails.email) {
                authority.getEmails().add(new Email(email, authority));
            }
        }

        airport.setId(xml_airport.airportID);
        airport.setAdName(xml_airport.adName);
        airport.setIata(xml_airport.iata);
        airport.setIcaoCountry(xml_airport.icaoCountry);
        airport.setDomesticId(xml_airport.domesticId);
        airport.setType(xml_airport.type);
        airport.setUsage(xml_airport.usage);
        airport.setAptRefPoint_dd(aptRefPoint_dd);
        airport.setAptRefPoint(aptRefPoint);
        airport.setDatumCode(xml_airport.datumCode);
        airport.setElevation(Integer.parseInt(xml_airport.elevation));
        airport.setIfr(xml_airport.ifr);
        airport.setAuthority(authority);
        airport.setHardSurface(xml_airport.hasHardSurface);
        airport.setCoordinate(location);

        if (xml_airport.towerFrequencies.towerFrequency != null) {
            for (String frequency : xml_airport.towerFrequencies.towerFrequency) {
                airport.getTowerFrequencies().add(new TowerFrequency(Double.parseDouble(frequency), airport));
            }
        }
        return airport;
    }

    private InputStream getXmlResource(String file) throws IOException {
        return new ClassPathResource("/skyy-xml/" + file + ".xml").getInputStream();
    }
}
