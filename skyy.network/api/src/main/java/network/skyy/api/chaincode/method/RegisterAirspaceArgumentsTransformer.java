package network.skyy.api.chaincode.method;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.AirspaceRequest;

public class RegisterAirspaceArgumentsTransformer implements MethodArgumentTransformer<AirspaceRequest> {

    @Override
    public String[] transform(AirspaceRequest source) throws JsonProcessingException {
        return new String[]{new ObjectMapper().writeValueAsString(source)};
    }

}
