package network.skyy.api.chaincode.method;

public class EmptyArgumentTransformer implements MethodArgumentTransformer<Object> {

    @Override
    public String[] transform(Object source) {
        return new String[]{};
    }

}
