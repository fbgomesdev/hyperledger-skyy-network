package network.skyy.api.model.airport;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode
public class PostAddressLine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String line;

    @ManyToOne
    @JoinColumn(name = "authority_id")
    private Authority authority;

    public PostAddressLine(String line, Authority authority) {
        this.line = line;
        this.authority = authority;
    }

}
