package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "operatorName",
        "certificate",
        "droneType",
        "altitude",
        "duration"
})
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SkyyLedgerData {

    @JsonProperty("operatorName")
    private String operatorName;

    @JsonProperty("certificate")
    private String certificate;

    @JsonProperty("droneType")
    private String droneType;

    @JsonProperty("altitude")
    private JSONAltitude altitude;

    @JsonProperty("duration")
    private JSONDuration duration;

}
