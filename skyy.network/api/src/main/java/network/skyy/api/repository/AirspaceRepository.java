package network.skyy.api.repository;

import network.skyy.api.model.airspace.Airspace;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AirspaceRepository extends CrudRepository<Airspace, Integer> {


    @Query(value = "SELECT * FROM Airspace arsp WHERE " +
            "st_dwithin(cast(arsp.polygon as geography), cast( ST_MakePoint(:longitude,:latitude) as geography),:range*1000,true) " +
            "AND (SELECT MIN(alt.value) from altitude_limit alt ,vertical_limit vlt WHERE vlt.airspace_id= arsp.id AND " +
            "vlt.lower_limit_id=alt.id)<:minimAlt",
            nativeQuery = true)
    List<Airspace> findAirspacesInRange(
            @Param("longitude") double longitude,
            @Param("latitude") double latitude,
            @Param("range") int range,
            @Param("minimAlt") int minAltitudeLowerThan);


}
