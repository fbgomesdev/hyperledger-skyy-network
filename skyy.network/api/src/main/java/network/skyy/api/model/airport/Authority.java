package network.skyy.api.model.airport;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@EqualsAndHashCode
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String aftn;

    private String sitaTelex;

    @OneToMany( mappedBy = "authority", cascade = CascadeType.ALL)
    private List<OperatorLine> operator=new ArrayList<>();

    @OneToMany( mappedBy = "authority", cascade = CascadeType.ALL)
    private List<Telephone> telephones=new ArrayList<>();

    @OneToMany( mappedBy = "authority", cascade = CascadeType.ALL)
    private List<Fax> faxes=new ArrayList<>();

    @OneToMany( mappedBy = "authority", cascade = CascadeType.ALL)
    private List<PostAddressLine> postAddress=new ArrayList<>();

    @OneToMany( mappedBy = "authority", cascade = CascadeType.ALL)
    private List<Email> emails=new ArrayList<>();

}
