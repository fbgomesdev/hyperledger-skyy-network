package network.skyy.api.service;

import network.skyy.api.chaincode.method.ChaincodeMethodType;
import network.skyy.api.json.ChaincodeInvokeResponse;
import network.skyy.api.json.ResponseStatus;
import network.skyy.fabric.FacadeFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChaincodeService {

    public ChaincodeInvokeResponse invokeWrite(ChaincodeMethodType requestType, Object arguments) {
        ChaincodeInvokeResponse response = ChaincodeInvokeResponse.builder()
                .status(ResponseStatus.SUCCESSFUL)
                .build();

        try {
            String[] stringArgs = requestType.getArgsTransformer().transform(arguments);
            String chaincodeName = requestType.getChaincode().getName();

            Optional<byte[]> chaincodeResponse = FacadeFactory.instance().invokeWrite(chaincodeName, requestType.name(), stringArgs);
            if (chaincodeResponse.isPresent()) {
                response.setPayload(requestType.getResponseTransformer().transform(chaincodeResponse.get()));
            }

        } catch (Exception e) {
            return ChaincodeInvokeResponse.builder()
                    .status(ResponseStatus.FAILED)
                    .message(e.getMessage())
                    .build();
        }

        return response;
    }

    public ChaincodeInvokeResponse invokeRead(ChaincodeMethodType requestType, Object arguments) {
        ChaincodeInvokeResponse response = ChaincodeInvokeResponse.builder()
                .status(ResponseStatus.SUCCESSFUL)
                .build();

        try {
            String[] stringArgs = requestType.getArgsTransformer().transform(arguments);
            String chaincodeName = requestType.getChaincode().getName();

            Optional<byte[]> chaincodeResponse = FacadeFactory.instance().invokeRead(chaincodeName, requestType.name(), stringArgs);

            if (chaincodeResponse.isPresent()) {
                response.setPayload(requestType.getResponseTransformer().transform(chaincodeResponse.get()));
            }

        } catch (Exception e) {
            return ChaincodeInvokeResponse.builder()
                    .message(e.getMessage())
                    .status(ResponseStatus.FAILED)
                    .build();
        }

        return response;
    }

}
