package network.skyy.api.chaincode.response;

import java.io.IOException;

public interface ResponseTransformer<R> {

    R transform(byte[] source) throws IOException;

}
