package network.skyy.api.model.airport;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vividsolutions.jts.geom.Point;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@EqualsAndHashCode
public class Airport {

    @Id
    private String id;

    private String adName;

    private String iata;

    private String icaoCountry;

    private String domesticId;

    private String type;

    private String usage;

    private String datumCode;

    private int elevation;

    private String ifr;

    private boolean HardSurface;

    @JsonIgnore
    private Point coordinate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "apt_ref_point_dd_id")
    private AptRefPoint_DD aptRefPoint_dd;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "apt_ref_point_id")
    private AptRefPoint aptRefPoint;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "authority_id")
    private Authority authority;

    @OneToMany( mappedBy = "airport", cascade = CascadeType.ALL)
    private List<TowerFrequency> towerFrequencies=new ArrayList<>();
}
