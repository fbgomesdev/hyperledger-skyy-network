package network.skyy.api.model.airspace;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@EqualsAndHashCode
public class VerticalLimit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String clazz;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="lower_limit_id")
    private AltitudeLimit lowerLimit;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="upper_limit_id")
    private AltitudeLimit upperLimit;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="airspace_id")
    private Airspace airspace;
}
