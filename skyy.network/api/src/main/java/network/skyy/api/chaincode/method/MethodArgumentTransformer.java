package network.skyy.api.chaincode.method;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface MethodArgumentTransformer<S> {

    String[] transform(S source) throws JsonProcessingException;

}
