package network.skyy.api.controller;

import network.skyy.fabric.FacadeFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ChaincodeController {

    @RequestMapping(path = "/write", method = RequestMethod.GET)
    @ResponseBody
    public String invokeWrite(String contract, String function, String[] arguments) {
        Optional<byte[]> result = FacadeFactory.instance().invokeWrite(contract, function, arguments);
        if (result.isPresent()) {
            return new String(result.get());
        }
        return "EMPTY";
    }


    @RequestMapping(path = "/read", method = RequestMethod.GET)
    @ResponseBody
    public String invokeRead(String contract, String function, String[] arguments) {
        Optional<byte[]> result = FacadeFactory.instance().invokeRead(contract, function, arguments);
        if (result.isPresent()) {
            return new String(result.get());
        }
        return "EMPTY";
    }

}
