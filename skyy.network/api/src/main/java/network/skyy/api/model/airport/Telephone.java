package network.skyy.api.model.airport;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode
public class Telephone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String number;

    @ManyToOne
    @JoinColumn(name = "authority_id")
    private Authority authority;

    public Telephone(String number, Authority authority) {
        this.number = number;
        this.authority = authority;
    }

}
