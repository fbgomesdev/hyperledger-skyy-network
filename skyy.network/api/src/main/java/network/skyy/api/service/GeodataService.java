package network.skyy.api.service;

import com.vividsolutions.jts.geom.Coordinate;
import network.skyy.api.json.*;
import network.skyy.api.model.airport.Airport;
import network.skyy.api.model.airport.OperatorLine;
import network.skyy.api.model.airspace.Airspace;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GeodataService {

    public Geodata create(List<Airspace> airspaces, List<Airport> airports) {
        return Geodata.builder()
                .flyspace(createFlyspace(airspaces, airports))
                .build();
    }

    private Flyspace createFlyspace(List<Airspace> airspaces, List<Airport> airports) {
        return Flyspace.builder()
                .circles(createCircleJSON(airports))
                .polygons(createPolygonJSON(airspaces))
                .build();

    }

    private List<FlyspaceCircle> createCircleJSON(List<Airport> airports) {
        // TODO
        List<FlyspaceCircle> circles = new ArrayList<>();
        for (Airport airport : airports) {
            List<Double> coordinates = new ArrayList<>();
            coordinates.add(airport.getCoordinate().getY());
            coordinates.add(airport.getCoordinate().getX());

            FlyspaceCircleData circleData = FlyspaceCircleData.builder()
                    .center(coordinates)
                    .radius(5000d)
                    .build();

            FlyspaceCircle circle = FlyspaceCircle.builder()
                    .circle(circleData)
                    .type(FlyspaceType.Danger)
                    .metadata(FlyspaceMetadata.builder().name(airport.getAdName())
                            .additionalProperties(convertAirportDataToMap(airport)).build())
                    .build();

            circles.add(circle);
        }
        return circles;
    }

    private List<FlyspacePolygon> createPolygonJSON(List<Airspace> airspaces) {
        List<FlyspacePolygon> array = new ArrayList<>();

        for (Airspace airspace : airspaces) {

            List<List<Double>> list = new ArrayList<>();
            for (Coordinate coordinate : airspace.getPolygon().getCoordinates()) {
                List<Double> list2 = new ArrayList<>();
                list.add(list2);
                list2.add(coordinate.y);
                list2.add(coordinate.x);
            }

            FlyspacePolygon polygon = FlyspacePolygon.builder()
                    .type(FlyspaceType.valueOf(airspace.getAirspaceType()))
                    .polygon(list)
                    .metadata(FlyspaceMetadata
                            .builder()
                            .name(airspace.getAirspaceName())
                            .additionalProperties(convertAirspaceDataToMap(airspace))
                            .build()
                    )
                    .build();
            array.add(polygon);
        }

        return array;
    }

    private Map<String, Object> convertAirspaceDataToMap(Airspace airspace) {
        Map<String, Object> map = new HashMap<>();

        map.put("Designator", airspace.getDesignator());
        map.put("Type", airspace.getAirspaceType());
        map.put("CountryName", airspace.getCountryName());
        map.put("LowerLimit", airspace.getVerticalLimits().get(0).getLowerLimit().getValue());
        if (airspace.getVerticalLimits().get(0).getLowerLimit().getUnit() != null)
            map.put("LLUnit", airspace.getVerticalLimits().get(0).getLowerLimit().getUnit());
        if (airspace.getVerticalLimits().get(0).getLowerLimit().getAltimeter() != null)
            map.put("LLAltimeter", airspace.getVerticalLimits().get(0).getLowerLimit().getAltimeter());
        map.put("UpperLimit", airspace.getVerticalLimits().get(0).getUpperLimit().getValue());
        if (airspace.getVerticalLimits().get(0).getUpperLimit().getUnit() != null)
            map.put("ULUnit", airspace.getVerticalLimits().get(0).getUpperLimit().getUnit());
        if (airspace.getVerticalLimits().get(0).getUpperLimit().getAltimeter() != null)
            map.put("ULAltimeter", airspace.getVerticalLimits().get(0).getUpperLimit().getAltimeter());


        return map;
    }

    private Map<String, Object> convertAirportDataToMap(Airport airport) {
        Map<String, Object> map = new HashMap<>();

        map.put("AirportID", airport.getId());
        map.put("ICAOCountry", airport.getIcaoCountry());
        String operatorLine = "";
        for (OperatorLine opLine : airport.getAuthority().getOperator()) {
            operatorLine += opLine.getLine() + "\n";
        }
        if (operatorLine.length() > 3)
            operatorLine = operatorLine.subSequence(0, operatorLine.length() - 3).toString();
        map.put("OperatorLine", operatorLine);

        return map;
    }
}
