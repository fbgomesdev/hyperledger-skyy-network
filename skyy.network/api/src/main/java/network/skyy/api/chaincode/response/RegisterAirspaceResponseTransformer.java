package network.skyy.api.chaincode.response;

public class RegisterAirspaceResponseTransformer implements ResponseTransformer<String> {
    @Override
    public String transform(byte[] source) {
        return new String(source);
    }
}
