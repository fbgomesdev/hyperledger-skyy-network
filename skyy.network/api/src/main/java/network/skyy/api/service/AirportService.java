package network.skyy.api.service;

import network.skyy.api.repository.AirportRepository;
import network.skyy.api.model.airport.Airport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportService {

    @Autowired
    private AirportRepository airportRepository;

    public void save(Airport airport) {
        airportRepository.save(airport);
    }

    public void saveAll(List<Airport> airports) {
        airportRepository.saveAll(airports);
    }

    public List<Airport> findAirportsInRange(double longitude, double latitude, int range) {
        return airportRepository.findAirportsInRange(longitude, latitude, range);
    }
}
