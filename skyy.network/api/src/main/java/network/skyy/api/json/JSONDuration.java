package network.skyy.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "unit"
})
@NoArgsConstructor
@AllArgsConstructor
@Data
public class JSONDuration {

    @JsonProperty("count")
    private int count;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("approvedTimestamp")
    private Timestamp approvedTimestamp;

    @JsonProperty("remainingMinutes")
    private int remainingMinutes;

}
