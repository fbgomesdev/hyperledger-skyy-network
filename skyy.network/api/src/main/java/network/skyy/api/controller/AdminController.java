package network.skyy.api.controller;


import network.skyy.api.chaincode.method.ChaincodeMethodType;
import network.skyy.api.json.AirspaceRequest;
import network.skyy.api.json.ChaincodeInvokeResponse;
import network.skyy.api.repository.AirspaceRepository;
import network.skyy.api.service.ChaincodeService;
import network.skyy.api.service.GeodataService;
import network.skyy.fabric.chaincode.manager.ChaincodeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;
import java.util.logging.Logger;
@RestController
@RequestMapping(value = "fabric")
public class AdminController {

    @Autowired
    AirspaceRepository airspaceRepository;

    @Autowired
    GeodataService geodataService;

    @Autowired
    ChaincodeService chaincodeService;

    @RequestMapping(value = "registerAirspace", method = RequestMethod.POST)
    public ChaincodeInvokeResponse registerAirspace(@RequestBody AirspaceRequest airspaceRequest) {
       return chaincodeService.invokeWrite(ChaincodeMethodType.REGISTER_AIRSPACE_REQUEST, airspaceRequest);

    }

    @RequestMapping(value = "getAirspacesToBeApproved", method = RequestMethod.GET)
    public ChaincodeInvokeResponse getAirspacesToBeApproved() {
        return chaincodeService.invokeRead(ChaincodeMethodType.GET_AIRSPACE_TO_APPROVE, null);
    }

    @RequestMapping(value = "getApprovedAirspaces", method = RequestMethod.GET)
    public ChaincodeInvokeResponse getApprovedAirspaces() {
        long totalSize = Runtime.getRuntime().totalMemory()/ (8*1000*1000);
        long maxSize = Runtime.getRuntime().maxMemory()/ (8*1000*1000);
        long freeSize = Runtime.getRuntime().freeMemory()/ (8*1000*1000);
        Logger.getLogger(AdminController.class.getName()).log(Level.INFO, new String("FreeMemory: "+ Integer.toString((int) freeSize)+ "MB of (" + Integer.toString((int) totalSize)+"MB/"+Integer.toString((int) maxSize)+"MB[max])"));
        //System.out.println("FreeMemory: "+ Integer.toString((int) freeSize)+ "MB of (" + Integer.toString((int) totalSize)+"MB/"+Integer.toString((int) maxSize)+"MB[max])");

        return chaincodeService.invokeWrite(ChaincodeMethodType.GET_APPROVED_AIRPACES, null);
    }

    @RequestMapping(value = "approveAirspace", method = RequestMethod.POST)
    public ChaincodeInvokeResponse approveAirspace(String id) {
        return chaincodeService.invokeWrite(ChaincodeMethodType.APPROVE_AIRSPACE, id);
    }

    @RequestMapping(value = "rejectAirspace", method = RequestMethod.POST)
    public ChaincodeInvokeResponse rejectAirspace(String id) {
        return chaincodeService.invokeWrite(ChaincodeMethodType.REJECT_AIRSPACE, id);
    }

    @RequestMapping(value = "status", method = RequestMethod.GET)
    public ChaincodeInvokeResponse getStatus(String id) {
        return chaincodeService.invokeRead(ChaincodeMethodType.GET_AIRSPACE_STATUS, id);
    }
}
