package network.skyy.api.controller;

import network.skyy.api.service.StorageService;
import network.skyy.fabric.FacadeFactory;
import org.hyperledger.fabric.sdk.TransactionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
public class FabricAdminController {


    @Autowired
    StorageService storageService;

    @RequestMapping(path = "/create", method = RequestMethod.GET)
    @ResponseBody
    public boolean createChannel() {
        return FacadeFactory.instance().createNewChannel();
    }

    @RequestMapping(path = "/enroll", method = RequestMethod.GET)
    public @ResponseBody
    boolean enroll() {
        return FacadeFactory.instance().registerEnrollUser();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = {"multipart/form-data"}, path = "/install")
    @ResponseBody
    public boolean install(
            @Valid
            @RequestParam("file")
                    MultipartFile file,
            @RequestParam(defaultValue = "skyyledger")
                    String contractName,
            @RequestParam(defaultValue = "JAVA")
                    TransactionRequest.Type type,
            @RequestParam(defaultValue = "1")
                    String chaincodeVersion,
            @RequestParam(defaultValue = "init")
                    String[] arguments) {

        String chaincodeRootDir = storageService.upload(file);
        if (chaincodeRootDir == null) {
            return false;
        }

        String path = null;
        if (!TransactionRequest.Type.JAVA.equals(type)) {
            path = "./";

        }

        return FacadeFactory.instance().deploy(contractName, path, chaincodeRootDir, type, chaincodeVersion, arguments);

    }


}
