package network.skyy.api.chaincode.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.AirspaceRequestStatus;

import java.io.IOException;

public class AirspaceStatusResponseTransformer implements ResponseTransformer<AirspaceRequestStatus> {
    @Override
    public AirspaceRequestStatus transform(byte[] source) throws IOException {
        return new ObjectMapper().readValue(source, AirspaceRequestStatus.class);
    }
}
