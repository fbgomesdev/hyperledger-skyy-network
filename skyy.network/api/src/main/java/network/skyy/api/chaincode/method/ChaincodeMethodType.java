package network.skyy.api.chaincode.method;

import lombok.Getter;
import network.skyy.api.chaincode.Chaincodes;
import network.skyy.api.chaincode.response.*;

public enum ChaincodeMethodType {
    REGISTER_AIRSPACE_REQUEST(Chaincodes.SKYY_LEDGER, RegisterAirspaceArgumentsTransformer.class, RegisterAirspaceResponseTransformer.class),
    GET_AIRSPACE_TO_APPROVE(Chaincodes.SKYY_LEDGER, EmptyArgumentTransformer.class, AirspaceRequestResponseTransformer.class),
    GET_APPROVED_AIRPACES(Chaincodes.SKYY_LEDGER, EmptyArgumentTransformer.class, ApprovedAirspacesResponseTransformer.class),
    APPROVE_AIRSPACE(Chaincodes.SKYY_LEDGER, StringArgumentTransformer.class, EmptyResponseTransformer.class),
    REJECT_AIRSPACE(Chaincodes.SKYY_LEDGER, StringArgumentTransformer.class, EmptyResponseTransformer.class),
    GET_AIRSPACE_STATUS(Chaincodes.SKYY_LEDGER, StringArgumentTransformer.class, AirspaceStatusResponseTransformer.class);

    @Getter
    private Chaincodes chaincode;

    private Class<?> argumentTransformer;

    private Class<?> responseTransformer;

    ChaincodeMethodType(Chaincodes chaincode, Class<?> argumentTransformer, Class<?> responseTransformer) {
        this.chaincode = chaincode;
        this.argumentTransformer = argumentTransformer;
        this.responseTransformer = responseTransformer;
    }

    public MethodArgumentTransformer getArgsTransformer() throws IllegalAccessException, InstantiationException {
        Object instance = this.argumentTransformer.newInstance();
        return instance != null ? (MethodArgumentTransformer) instance : null;
    }

    public ResponseTransformer getResponseTransformer() throws IllegalAccessException, InstantiationException {
        Object instance = this.responseTransformer.newInstance();
        return instance != null ? (ResponseTransformer) instance : null;
    }
}
