package network.skyy.api.chaincode.response;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.AirspaceRequest;

import java.io.IOException;
import java.util.List;

public class AirspaceRequestResponseTransformer implements ResponseTransformer<List<AirspaceRequest>> {

    @Override
    public List<AirspaceRequest> transform(byte[] source) {
        try {
            return new ObjectMapper().readValue(source, new TypeReference<List<AirspaceRequest>>() {
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
