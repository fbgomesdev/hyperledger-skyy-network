package network.skyy.api.chaincode.response;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import network.skyy.api.json.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApprovedAirspacesResponseTransformer implements ResponseTransformer<Geodata> {

    @Override
    public Geodata transform(byte[] source) {
        try {
            List<AirspaceRequest> approvedAirspaces = new ObjectMapper().readValue(source, new TypeReference<List<AirspaceRequest>>() {
            });
            Geodata geodata = Geodata.builder()
                    .build();

            geodata.setFlyspace(Flyspace.builder()
                    .circles(new ArrayList<>())
                    .polygons(new ArrayList<>())
                    .build());

            approvedAirspaces.stream().forEach(airspaceRequest -> {
                List<FlyspaceCircle> circles = airspaceRequest.getFlyspace().getCircles();

                circles.stream().forEach(c -> {
                            c.setMetadata(FlyspaceMetadata.builder()
                                    .name(airspaceRequest.getSkyyLedgerData().getOperatorName())
                                    .additionalProperties(convertToMap(airspaceRequest.getSkyyLedgerData()))
                                    .build());
                            c.setMinAlt(airspaceRequest.getSkyyLedgerData().getAltitude().getMin());
                        }
                );

                geodata.getFlyspace().getCircles().addAll(circles);

                List<FlyspacePolygon> polygons = airspaceRequest.getFlyspace().getPolygons();

                polygons.stream().forEach(p -> {
                            p.setMetadata(FlyspaceMetadata.builder()
                                    .name(airspaceRequest.getSkyyLedgerData().getOperatorName())
                                    .additionalProperties(convertToMap(airspaceRequest.getSkyyLedgerData()))
                                    .build());
                            p.setMinAlt(airspaceRequest.getSkyyLedgerData().getAltitude().getMin());
                        }
                );
                geodata.getFlyspace().getPolygons().addAll(polygons);

            });

            return geodata;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, Object> convertToMap(SkyyLedgerData data) {
        Map<String, Object> map = new HashMap<>();

        String min = Integer.toString(data.getAltitude().getMin());
        String max = Integer.toString(data.getAltitude().getMax());
        String unit = data.getAltitude().getUnit();
        map.put("Altitude", min + ":" + max + " " + unit);

        String time = String.valueOf(data.getDuration().getRemainingMinutes());
        String time_unit = data.getDuration().getUnit();
        map.put("Duration", time + " " + time_unit + " remaining");
        map.put("Type",data.getDroneType());
        map.put("Certificate",data.getCertificate());

        return map;
    }

}
