package network.skyy.api.chaincode.method;

public class StringArgumentTransformer implements MethodArgumentTransformer<Object> {

    @Override
    public String[] transform(Object source) {
        return new String[]{String.valueOf(source)};
    }

}
