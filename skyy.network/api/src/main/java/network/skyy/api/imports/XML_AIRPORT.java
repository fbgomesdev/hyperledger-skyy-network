package network.skyy.api.imports;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "airportDbId",
        "airportID",
        "adName",
        "iata",
        "icaoCountry",
        "domesticId",
        "type",
        "usage",
        "aptRefPointDD",
        "aptRefPoint",
        "datumCode",
        "elevation",
        "ifr",
        "authorities",
        "towerFrequencies",
        "hasHardSurface",
        "runways",
        "helipads"
})

@Getter
@Setter
public class XML_AIRPORT {

    @XmlElement(name = "AirportDbId")
    @Id
    protected BigInteger airportDbId;
    @XmlElement(name = "AirportID", required = true)
    protected String airportID;
    @XmlElement(name = "ADName", required = true)
    protected String adName;
    @XmlElement(name = "IATA", required = true)
    protected String iata;
    @XmlElement(name = "ICAOCountry", required = true)
    protected String icaoCountry;
    @XmlElement(name = "DomesticId", required = true)
    protected String domesticId;
    @XmlElement(name = "Type", required = true)
    protected String type;
    @XmlElement(name = "Usage", required = true)
    protected String usage;
    @XmlElement(name = "AptRefPoint_DD", required = true)
    protected XML_AIRPORT.AptRefPointDD aptRefPointDD;
    @XmlElement(name = "AptRefPoint", required = true)
    protected XML_AIRPORT.AptRefPoint aptRefPoint;
    @XmlElement(name = "DatumCode", required = true)
    protected String datumCode;
    @XmlElement(name = "Elevation", required = true)
    protected String elevation;
    @XmlElement(name = "IFR", required = true)
    protected String ifr;
    @XmlElement(name = "Authorities", required = true)
    protected XML_AIRPORT.Authorities authorities;
    @XmlElement(name = "TowerFrequencies", required = true)
    protected XML_AIRPORT.TowerFrequencies towerFrequencies;
    @XmlElement(name = "HasHardSurface")
    protected boolean hasHardSurface;
    @XmlElement(name = "Runways")
    protected XML_AIRPORT.Runways runways;
    @XmlElement(name = "Helipads")
    protected XML_AIRPORT.Helipads helipads;




    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "latitude",
            "longitude"
    })
    @Getter
    @Setter
    public static class AptRefPoint {

        @XmlElement(name = "Latitude", required = true)
        protected String latitude;
        @XmlElement(name = "Longitude", required = true)
        protected String longitude;

    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "latitudeDD",
            "longitudeDD"
    })
    @Getter @Setter
    public static class AptRefPointDD {

        @XmlElement(name = "LatitudeDD", required = true)
        protected String latitudeDD;
        @XmlElement(name = "LongitudeDD", required = true)
        protected String longitudeDD;


    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "operator",
            "telephones",
            "faxes",
            "aftn",
            "sitaTelex",
            "postAdress",
            "emails"
    })
    @Getter
    @Setter
    public static class Authorities {

        @XmlElement(name = "Operator", required = true)
        protected XML_AIRPORT.Authorities.Operator operator;
        @XmlElement(name = "Telephones", required = true)
        protected XML_AIRPORT.Authorities.Telephones telephones;
        @XmlElement(name = "Faxes", required = true)
        protected XML_AIRPORT.Authorities.Faxes faxes;
        @XmlElement(name = "AFTN", required = true)
        protected String aftn;
        @XmlElement(name = "SITA_Telex", required = true)
        protected String sitaTelex;
        @XmlElement(name = "PostAdress", required = true)
        protected XML_AIRPORT.Authorities.PostAdress postAdress;
        @XmlElement(name = "Emails", required = true)
        protected XML_AIRPORT.Authorities.Emails emails;


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "email"
        })
        @Getter @Setter
        public static class Emails {

            @XmlElement(name = "Email")
            protected List<String> email;

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "fax"
        })
        @Getter @Setter
        public static class Faxes {

            @XmlElement(name = "Fax")
            protected List<String> fax;

        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "operatorLine"
        })
        @Getter @Setter
        public static class Operator {

            @XmlElement(name = "OperatorLine")
            protected List<String> operatorLine;

        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "addressLine"
        })
        @Getter @Setter
        public static class PostAdress {

            @XmlElement(name = "AddressLine")
            protected List<String> addressLine;

        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "telephone"
        })
        @Getter @Setter
        public static class Telephones {

            @XmlElement(name = "Telephone")
            protected List<String> telephone;

        }

    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "helipad"
    })
    @Getter @Setter
    public static class Helipads {

        @XmlElement(name = "Helipad")
        protected List<XML_AIRPORT.Helipads.Helipad> helipad;

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "helipadDbId",
                "helipadID",
                "helipadCoordinates",
                "helipadCoordinatesDD"
        })
        @Getter
        @Setter
        public static class Helipad {

            @XmlElement(name = "HelipadDbId", required = true)
            protected BigInteger helipadDbId;
            @XmlElement(name = "HelipadID", required = true)
            protected String helipadID;
            @XmlElement(name = "Helipad_Coordinates", required = true)
            protected XML_AIRPORT.Helipads.Helipad.HelipadCoordinates helipadCoordinates;
            @XmlElement(name = "Helipad_CoordinatesDD", required = true)
            protected XML_AIRPORT.Helipads.Helipad.HelipadCoordinatesDD helipadCoordinatesDD;


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "helipadLatitude",
                    "helipadLongitude"
            })
            public static class HelipadCoordinates {

                @XmlElement(name = "Helipad_Latitude", required = true)
                protected String helipadLatitude;
                @XmlElement(name = "Helipad_Longitude", required = true)
                protected String helipadLongitude;

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "helipadLatitudeDD",
                    "helipadLongitudeDD"
            })
            @Getter @Setter
            public static class HelipadCoordinatesDD {

                @XmlElement(name = "Helipad_LatitudeDD", required = true)
                protected BigDecimal helipadLatitudeDD;
                @XmlElement(name = "Helipad_LongitudeDD", required = true)
                protected BigDecimal helipadLongitudeDD;


            }

        }

    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "runway"
    })
    @Getter @Setter
    public static class Runways {

        @XmlElement(name = "Runway")
        protected List<XML_AIRPORT.Runways.Runway> runway;

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "rwyDbId",
                "runwayID",
                "rwyThrLat",
                "rwyThrLon",
                "length",
                "trueBearing",
                "magneticBearing",
                "tora",
                "toda",
                "asda",
                "lda",
                "slope"
        })
        @Getter @Setter
        public static class Runway {

            @XmlElement(name = "RwyDbId")
            protected BigInteger rwyDbId;
            @XmlElement(name = "RunwayID", required = true)
            protected String runwayID;
            @XmlElement(name = "RwyThrLat", required = true)
            protected String rwyThrLat;
            @XmlElement(name = "RwyThrLon", required = true)
            protected String rwyThrLon;
            @XmlElement(name = "Length")
            @XmlSchemaType(name = "unsignedShort")
            protected int length;
            @XmlElement(name = "TrueBearing", required = true)
            protected String trueBearing;
            @XmlElement(name = "MagneticBearing")
            protected int magneticBearing;
            @XmlElement(name = "TORA")
            protected Integer tora;
            @XmlElement(name = "TODA")
            protected Integer toda;
            @XmlElement(name = "ASDA")
            protected Integer asda;
            @XmlElement(name = "LDA")
            protected Integer lda;
            @XmlElement(name = "Slope", required = true)
            protected BigDecimal slope;

        }

    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "towerFrequency"
    })
    @Getter @Setter
    public static class TowerFrequencies {

        @XmlElement(name = "TowerFrequency")
        protected List<String> towerFrequency;

    }

}