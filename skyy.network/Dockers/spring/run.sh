#!/bin/bash
set -e
mvn -f /home/app/pom.xml clean package -Dmaven.test.skip=true
cp /home/app/api/target/api-*.jar /usr/local/lib/app.jar
cp -r /home/app/network/src/main/resources/* /usr/local/lib/
cd /usr/local/lib/
jar xf app.jar
java -Dspring.profiles.active=production -Dmode=exploded org.springframework.boot.loader.JarLauncher
#java -cp /lib/*:/ org.springframework.boot.loader.JarLauncher -Dspring.profiles.active=production -Dmode=exploded
#java -cp /lib/*:/ network.skyy.api.ApiApplication
#java -cp ./com/*:./org/* network.skyy.api.ApiApplication
#java -jar -Dspring.profiles.active=production app.jar