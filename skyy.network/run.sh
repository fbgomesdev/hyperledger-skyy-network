#!/bin/bash
set -e
mvn -f ./pom.xml clean package -Dmaven.test.skip=true
cp ./api/target/api-*.jar /usr/local/lib/app.jar
cp -r ./network/src/main/resources/* /usr/local/lib/
cd /usr/local/lib/
jar xf app.jar
java -Xmx12192M -Xms12192M -Dmode=exploded org.springframework.boot.loader.JarLauncher -X
#java -cp /lib/*:/ org.springframework.boot.loader.JarLauncher -Dspring.profiles.active=production -Dmode=exploded
#java -cp /lib/*:/ network.skyy.api.ApiApplication
#java -cp ./com/*:./org/* network.skyy.api.ApiApplication
#java -jar -Dspring.profiles.active=production app.jar			