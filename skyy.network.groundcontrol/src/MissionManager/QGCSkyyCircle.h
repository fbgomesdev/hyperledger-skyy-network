/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

#pragma once

#include "QGCMapCircle.h"

/// The QGCSkyyCircle class provides a cicle used by GeoFence support.
class QGCSkyyCircle : public QGCMapCircle
{
    Q_OBJECT

public:
    QGCSkyyCircle(QObject* parent = NULL);
    QGCSkyyCircle(const QGeoCoordinate& center, double radius, bool inclusion, QObject* parent = NULL);
    QGCSkyyCircle(const QGCSkyyCircle& other, QObject* parent = NULL);

    const QGCSkyyCircle& operator=(const QGCSkyyCircle& other);

    Q_PROPERTY(bool inclusion READ inclusion WRITE setInclusion NOTIFY inclusionChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString metaData READ metaData WRITE setMetaData NOTIFY metaDataChanged)
    Q_PROPERTY(int minAlt READ minAlt WRITE setMinAlt NOTIFY minAltChanged)

    /// Saves the QGCSkyyCircle to the json object.
    ///     @param json Json object to save to
    void saveToJson(QJsonObject& json);

    /// Load a QGCSkyyCircle from json
    ///     @param json Json object to load from
    ///     @param errorString Error string if return is false
    /// @return true: success, false: failure (errorString set)
    bool loadFromJson(const QJsonObject& json, QString& errorString);

    // Property methods

    bool inclusion      (void) const { return _inclusion; }
    QString type        (void) const { return _type; }
    QString name        (void) const { return _name; }
    QString metaData    (void) const { return _metaData; }
    int minAlt          (void) const { return _minAlt; }
    void setInclusion   (bool inclusion);
    void setType        (QString type);
    void setName        (QString name);
    void setMetaData    (QString metaData);
    void setMinAlt      (int minAlt);

signals:
    void inclusionChanged   (bool inclusion);
    void typeChanged        (QString type);
    void nameChanged        (QString name);
    void metaDataChanged    (QString metaData);
    void minAltChanged      (int minAlt);

private slots:
    void _setDirty(void);

private:
    void _init(void);

    bool _inclusion;

    QString _type;

    QString _name;

    QString _metaData;

    int _minAlt;

    static const int _jsonCurrentVersion = 1;

    static const char* _jsonInclusionKey;
    static const char* _jsonTypeKey;
    static const char* _jsonMetaDataKey;
    static const char* _jsonNameKey;
    static const char* _jsonMinAltKey;
};
