/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

#include "QGCSkyyCircle.h"
#include "JsonHelper.h"

#include <QJsonDocument>

const char* QGCSkyyCircle::_jsonInclusionKey = "inclusion";
const char* QGCSkyyCircle::_jsonTypeKey = "type";
const char* QGCSkyyCircle::_jsonMetaDataKey = "metadata";
const char* QGCSkyyCircle::_jsonNameKey = "name";
const char* QGCSkyyCircle::_jsonMinAltKey = "minAlt";

QGCSkyyCircle::QGCSkyyCircle(QObject* parent)
    : QGCMapCircle  (parent)
    , _inclusion    (true)
{
    _init();
}

QGCSkyyCircle::QGCSkyyCircle(const QGeoCoordinate& center, double radius, bool inclusion, QObject* parent)
    : QGCMapCircle  (center, radius, false /* showRotation */, true /* clockwiseRotation */, parent)
    , _inclusion    (inclusion)
{
    _init();
}

QGCSkyyCircle::QGCSkyyCircle(const QGCSkyyCircle& other, QObject* parent)
    : QGCMapCircle  (other, parent)
    , _inclusion    (other._inclusion)
{
    _init();
}

void QGCSkyyCircle::_init(void)
{
    connect(this, &QGCSkyyCircle::inclusionChanged, this, &QGCSkyyCircle::_setDirty);
}

const QGCSkyyCircle& QGCSkyyCircle::operator=(const QGCSkyyCircle& other)
{
    QGCMapCircle::operator=(other);

    setInclusion(other._inclusion);

    return *this;
}

void QGCSkyyCircle::_setDirty(void)
{
    setDirty(true);
}

void QGCSkyyCircle::saveToJson(QJsonObject& json)
{
    json[JsonHelper::jsonVersionKey] = _jsonCurrentVersion;
    json[_jsonInclusionKey] = _inclusion;
    QGCMapCircle::saveToJson(json);
}

bool QGCSkyyCircle::loadFromJson(const QJsonObject& json, QString& errorString)
{
    errorString.clear();

    if (!QGCMapCircle::loadFromJson(json, errorString)) {
        return false;
    }

    QString defType = "no-fly";
    if (json.contains(_jsonTypeKey))
        defType = json[_jsonTypeKey].toString();

    QString defName = defType;
    QString defMetaData = "";
    if (json.contains(_jsonMetaDataKey)) {
        QJsonObject newObj = json[_jsonMetaDataKey].toObject();

        if(newObj.contains(_jsonNameKey))
            defName = newObj[_jsonNameKey].toString();

        QJsonDocument doc(newObj);
        defMetaData = doc.toJson(QJsonDocument::Compact);
    }

    int defMinAlt = 0;
    if (json.contains(_jsonMinAltKey))
        defMinAlt = json[_jsonMinAltKey].toInt();

    setMinAlt(defMinAlt);
    setType(defType);
    setName(defName);
    setMetaData(defMetaData);

    return true;
}

void QGCSkyyCircle::setInclusion(bool inclusion)
{
    if (inclusion != _inclusion) {
        _inclusion = inclusion;
        emit inclusionChanged(inclusion);
    }
}

void QGCSkyyCircle::setType(QString type)
{
    if (type != _type) {
        _type = type;
        emit typeChanged(type);
    }
}

void QGCSkyyCircle::setName(QString name)
{
    if (name != _name) {
        _name = name;
        emit nameChanged(name);
    }
}

void QGCSkyyCircle::setMetaData(QString metaData)
{
    if (metaData != _metaData) {
        _metaData = metaData;
        emit metaDataChanged(metaData);
    }
}

void QGCSkyyCircle::setMinAlt(int minAlt)
{
    if (minAlt != _minAlt) {
        _minAlt = minAlt;
        emit minAltChanged(minAlt);
    }
}