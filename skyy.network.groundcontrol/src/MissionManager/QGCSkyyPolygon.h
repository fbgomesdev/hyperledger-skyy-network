/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

#pragma once

#include "QGCMapPolygon.h"

/// The QGCSkyyPolygon class provides a polygon used by SkyyLedger support.
class QGCSkyyPolygon : public QGCMapPolygon
{
    Q_OBJECT

public:
    QGCSkyyPolygon(bool inclusion, QObject* parent = NULL);
    QGCSkyyPolygon(const QGCSkyyPolygon& other, QObject* parent = NULL);

    const QGCSkyyPolygon& operator=(const QGCSkyyPolygon& other);

    Q_PROPERTY(bool inclusion READ inclusion WRITE setInclusion NOTIFY inclusionChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString metaData READ metaData WRITE setMetaData NOTIFY metaDataChanged)
    Q_PROPERTY(int minAlt READ minAlt WRITE setMinAlt NOTIFY minAltChanged)
    Q_PROPERTY(double area READ area WRITE setArea NOTIFY areaChanged)

    /// Saves the QGCSkyyPolygon to the json object.
    ///     @param json Json object to save to
    void saveToJson(QJsonObject& json);

    /// Load a QGCSkyyPolygon from json
    ///     @param json Json object to load from
    ///     @param required true: no polygon in object will generate error
    ///     @param errorString Error string if return is false
    /// @return true: success, false: failure (errorString set)
    bool loadFromJson(const QJsonObject& json, bool required, QString& errorString);

    // Property methods

    bool inclusion      (void) const { return _inclusion; }
    QString type        (void) const { return _type; }
    QString name        (void) const { return _name; }
    QString metaData    (void) const { return _metaData; }
    int minAlt          (void) const { return _minAlt; }
    double area         (void) const { return _area; }
    void setInclusion   (bool inclusion);
    void setType        (QString type);
    void setName        (QString name);
    void setMetaData    (QString metaData);
    void setMinAlt      (int minAlt);
    void setArea        (double area);

signals:
    void inclusionChanged   (bool inclusion);
    void typeChanged        (QString type);
    void nameChanged        (QString name);
    void metaDataChanged    (QString metaData);
    void minAltChanged      (int minAlt);
    void areaChanged        (double area);

private slots:
    void _setDirty(void);

private:
    void _init(void);

    bool _inclusion;

    QString _type;

    QString _name;

    QString _metaData;

    int _minAlt;

    double _area;

    static const int _jsonCurrentVersion = 1;

    static const char* _jsonInclusionKey;
    static const char* _jsonTypeKey;
    static const char* _jsonMetaDataKey;
    static const char* _jsonNameKey;
    static const char* _jsonMinAltKey;
};
