/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/


/// @file
///     @author Don Gagne <don@thegagnes.com>

#include "SkyyLedgerController.h"
#include "Vehicle.h"
#include "FirmwarePlugin.h"
#include "MAVLinkProtocol.h"
#include "QGCApplication.h"
#include "ParameterManager.h"
#include "SettingsManager.h"
#include "JsonHelper.h"
#include "QGCQGeoCoordinate.h"
#include "AppSettings.h"
#include "PlanMasterController.h"

#ifndef __mobile__
#include "MainWindow.h"
#include "QGCQFileDialog.h"
#endif

#include <QJsonDocument>
#include <QJsonArray>

QGC_LOGGING_CATEGORY(SkyyLedgerControllerLog, "SkyyLedgerControllerLog")

const char* SkyyLedgerController::_jsonFileTypeValue =        "flyspace";
const char* SkyyLedgerController::_jsonBreachReturnKey =      "breachReturn";
const char* SkyyLedgerController::_jsonPolygonsKey =          "polygons";
const char* SkyyLedgerController::_jsonCirclesKey =           "circles";

const char* SkyyLedgerController::_px4ParamCircularFence =    "GF_MAX_HOR_DIST";

SkyyLedgerController::SkyyLedgerController(PlanMasterController* masterController, QObject* parent)
    : PlanElementController     (masterController, parent)
    , _geoFenceManager          (_managerVehicle->geoFenceManager())
    , _dirty                    (false)
    , _itemsRequested           (false)
    , _px4ParamCircularFenceFact(NULL)
{
    connect(&_polygons, &QmlObjectListModel::countChanged, this, &SkyyLedgerController::_updateContainsItems);
    connect(&_circles,  &QmlObjectListModel::countChanged, this, &SkyyLedgerController::_updateContainsItems);

    managerVehicleChanged(_managerVehicle);
}

SkyyLedgerController::~SkyyLedgerController()
{

}

void SkyyLedgerController::start(bool flyView)
{
    qCDebug(SkyyLedgerControllerLog) << "start flyView" << flyView;

    PlanElementController::start(flyView);
    _init();
}

void SkyyLedgerController::_init(void)
{

}

void SkyyLedgerController::setBreachReturnPoint(const QGeoCoordinate& breachReturnPoint)
{
    if (_breachReturnPoint != breachReturnPoint) {
        _breachReturnPoint = breachReturnPoint;
        setDirty(true);
        emit breachReturnPointChanged(breachReturnPoint);
    }
}

void SkyyLedgerController::_signalAll(void)
{
    emit breachReturnPointChanged(breachReturnPoint());
    emit dirtyChanged(dirty());
    emit supportedChanged(supported());
}

void SkyyLedgerController::managerVehicleChanged(Vehicle* managerVehicle)
{
    if (_managerVehicle) {
        _geoFenceManager->disconnect(this);
        _managerVehicle->disconnect(this);
        _managerVehicle->parameterManager()->disconnect(this);
        _managerVehicle = NULL;
        _geoFenceManager = NULL;
    }

    _managerVehicle = managerVehicle;
    if (!_managerVehicle) {
        qWarning() << "SkyyLedgerController::managerVehicleChanged managerVehicle=NULL";
        return;
    }

    _geoFenceManager = _managerVehicle->geoFenceManager();
    connect(_geoFenceManager, &GeoFenceManager::loadComplete,                   this, &SkyyLedgerController::_managerLoadComplete);
    connect(_geoFenceManager, &GeoFenceManager::sendComplete,                   this, &SkyyLedgerController::_managerSendComplete);
    connect(_geoFenceManager, &GeoFenceManager::removeAllComplete,              this, &SkyyLedgerController::_managerRemoveAllComplete);
    connect(_geoFenceManager, &GeoFenceManager::inProgressChanged,              this, &SkyyLedgerController::syncInProgressChanged);

    connect(_managerVehicle,  &Vehicle::capabilityBitsChanged,                  this, &SkyyLedgerController::supportedChanged);

    connect(_managerVehicle->parameterManager(), &ParameterManager::parametersReadyChanged, this, &SkyyLedgerController::_parametersReady);
    _parametersReady();

    emit supportedChanged(supported());
    _signalAll();
}

bool SkyyLedgerController::load(const QJsonObject& json, QString& errorString)
{
    return load(json, "general", errorString);
}

QStringList SkyyLedgerController::getAvailableTypes() {
    QStringList typeList;
    typeList << "pending" << "approved" << "no-fly" << "skyy-ledger" << "active";

    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeProhibited()->rawValue().toBool())
        typeList << "Prohibited";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeRestricted()->rawValue().toBool())
        typeList << "Restricted";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeDanger()->rawValue().toBool())
        typeList << "Danger";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeCTA()->rawValue().toBool())
        typeList << "CTA";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeWarning()->rawValue().toBool())
        typeList << "Warning";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeOtherSUAS()->rawValue().toBool())
        typeList << "OtherSUAS";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeMOA()->rawValue().toBool())
        typeList << "MOA";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeADIZ()->rawValue().toBool())
        typeList << "ADIZ";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeOCA()->rawValue().toBool())
        typeList << "OCA";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeFuelDumpingArea()->rawValue().toBool())
        typeList << "FuelDumpingArea";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeCTR()->rawValue().toBool())
        typeList << "CTR";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeAlert()->rawValue().toBool())
        typeList << "Alert";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeTsaTraCba()->rawValue().toBool())
        typeList << "TsaTraCba";
    if(qgcApp()->toolbox()->settingsManager()->appSettings()->showTypeTMA()->rawValue().toBool())
        typeList << "TMA";
    return typeList;
}

bool SkyyLedgerController::load(const QJsonObject& json, QString type, QString& errorString)
{
    if(type == "pending") {
        _pendingCircles.clearAndDeleteContents();
        _pendingPolygons.clearAndDeleteContents();
    } else if(type == "approved") {
        _ledgerCircles.clearAndDeleteContents();
        _ledgerPolygons.clearAndDeleteContents();
    } else {
        removeAll();
    }

    errorString.clear();

    QStringList typeList = getAvailableTypes();

    if (json.contains(_jsonPolygonsKey)) {
        QJsonArray jsonPolygonArray = json[_jsonPolygonsKey].toArray();
        for (const QJsonValue &jsonPolygonValue: jsonPolygonArray) {
            if (jsonPolygonValue.type() != QJsonValue::Object) {
                errorString = tr("GeoFence polygon not stored as object");
                return false;
            }

            QGCSkyyPolygon *skyyPolygon = new QGCSkyyPolygon(false /* inclusion */, this /* parent */);
            if (!skyyPolygon->loadFromJson(jsonPolygonValue.toObject(), true /* required */, errorString)) {
                return false;
            }

            if(typeList.indexOf(skyyPolygon->type()) == -1)
                continue;

            if(type == "pending") {
                _pendingPolygons.append(skyyPolygon);
            } else if(type == "approved") {
                _ledgerPolygons.append(skyyPolygon);
            } else {
                if (skyyPolygon->minAlt() <= 400) {
                    _polygons.append(skyyPolygon);
                }
                _allPolygons.append(skyyPolygon);
            }
        }
    }

    if (json.contains(_jsonCirclesKey)) {
        QJsonArray jsonCircleArray = json[_jsonCirclesKey].toArray();
        for (const QJsonValue &jsonCircleValue: jsonCircleArray) {
            if (jsonCircleValue.type() != QJsonValue::Object) {
                errorString = tr("GeoFence circle not stored as object");
                return false;
            }

            QGCSkyyCircle *skyyCircle = new QGCSkyyCircle(this /* parent */);
            if (!skyyCircle->loadFromJson(jsonCircleValue.toObject(), errorString)) {
                return false;
            }

            if(typeList.indexOf(skyyCircle->type()) == -1)
                continue;

            if(type == "pending") {
                _pendingCircles.append(skyyCircle);
            } else if(type == "approved") {
                _ledgerCircles.append(skyyCircle);
            } else {
                if (skyyCircle->minAlt() <= 400) {
                    _circles.append(skyyCircle);
                }
                _allCircles.append(skyyCircle);
            }
        }
    }

    setDirty(false);
    _signalAll();

    return true;
}

void SkyyLedgerController::save(QJsonObject& json)
{
    json[JsonHelper::jsonVersionKey] = _jsonCurrentVersion;

    QJsonArray jsonPolygonArray;
    for (int i=0; i<_polygons.count(); i++) {
        QJsonObject jsonPolygon;
        QGCSkyyPolygon* fencePolygon = _polygons.value<QGCSkyyPolygon*>(i);
        fencePolygon->saveToJson(jsonPolygon);
        jsonPolygonArray.append(jsonPolygon);
    }
    json[_jsonPolygonsKey] = jsonPolygonArray;

    QJsonArray jsonCircleArray;
    for (int i=0; i<_circles.count(); i++) {
        QJsonObject jsonCircle;
        QGCSkyyCircle* fenceCircle = _circles.value<QGCSkyyCircle*>(i);
        fenceCircle->saveToJson(jsonCircle);
        jsonCircleArray.append(jsonCircle);
    }
    json[_jsonCirclesKey] = jsonCircleArray;
}

void SkyyLedgerController::removeAll(void)
{
    setBreachReturnPoint(QGeoCoordinate());
    _polygons.clearAndDeleteContents();
    _allPolygons.clearAndDeleteContents();
    _circles.clearAndDeleteContents();
    _allCircles.clearAndDeleteContents();
}

void SkyyLedgerController::removeAllFromVehicle(void)
{
    if (_masterController->offline()) {
        qCWarning(SkyyLedgerControllerLog) << "SkyyLedgerController::removeAllFromVehicle called while offline";
    } else if (syncInProgress()) {
        qCWarning(SkyyLedgerControllerLog) << "SkyyLedgerController::removeAllFromVehicle called while syncInProgress";
    } else {
        _geoFenceManager->removeAll();
    }
}

void SkyyLedgerController::loadFromVehicle(void)
{
    if (_masterController->offline()) {
        qCWarning(SkyyLedgerControllerLog) << "SkyyLedgerController::loadFromVehicle called while offline";
    } else if (syncInProgress()) {
        qCWarning(SkyyLedgerControllerLog) << "SkyyLedgerController::loadFromVehicle called while syncInProgress";
    } else {
        _itemsRequested = true;
        _geoFenceManager->loadFromVehicle();
    }
}

void SkyyLedgerController::sendToVehicle(void)
{
    if (_masterController->offline()) {
        qCWarning(SkyyLedgerControllerLog) << "SkyyLedgerController::sendToVehicle called while offline";
    } else if (syncInProgress()) {
        qCWarning(SkyyLedgerControllerLog) << "SkyyLedgerController::sendToVehicle called while syncInProgress";
    } else {
        qCDebug(SkyyLedgerControllerLog) << "SkyyLedgerController::sendToVehicle";
        _geoFenceManager->sendToVehicle(_breachReturnPoint, _polygons, _circles);
        setDirty(false);
    }
}

bool SkyyLedgerController::syncInProgress(void) const
{
    return _geoFenceManager->inProgress();
}

bool SkyyLedgerController::dirty(void) const
{
    return _dirty;
}


void SkyyLedgerController::setDirty(bool dirty)
{
    if (dirty != _dirty) {
        _dirty = dirty;
        if (!dirty) {
            for (int i=0; i<_polygons.count(); i++) {
                QGCSkyyPolygon* polygon = _polygons.value<QGCSkyyPolygon*>(i);
                polygon->setDirty(false);
            }
            for (int i=0; i<_circles.count(); i++) {
                QGCSkyyCircle* circle = _circles.value<QGCSkyyCircle*>(i);
                circle->setDirty(false);
            }
        }
        emit dirtyChanged(dirty);
    }
}

void SkyyLedgerController::_polygonDirtyChanged(bool dirty)
{
    if (dirty) {
        setDirty(true);
    }
}

void SkyyLedgerController::_setDirty(void)
{
    setDirty(true);
}

void SkyyLedgerController::_setFenceFromManager(const QList<QGCFencePolygon>& polygons,
                                              const QList<QGCFenceCircle>&  circles)
{
    qWarning() << "call commented method: _setFenceFromManager";
    _polygons.clearAndDeleteContents();
    _circles.clearAndDeleteContents();
    // TODO - RaduP - Do we need this method anyway?

//    for (int i=0; i<polygons.count(); i++) {
//        _polygons.append(new QGCSkyyPolygon(polygons[i], this));
//    }
//
//    for (int i=0; i<circles.count(); i++) {
//        _circles.append(new QGCFenceCircle(circles[i], this));
//    }

    setDirty(false);
}

void SkyyLedgerController::_setReturnPointFromManager(QGeoCoordinate breachReturnPoint)
{
    _breachReturnPoint = breachReturnPoint;
    emit breachReturnPointChanged(_breachReturnPoint);
}

void SkyyLedgerController::_managerLoadComplete(void)
{
    qWarning() << "call commented method: _managerLoadComplete";

    // TODO - RaduP - Do we need this method anyway?

    // Fly view always reloads on _loadComplete
    // Plan view only reloads on _loadComplete if specifically requested
//    if (_flyView || _itemsRequested) {
//        _setReturnPointFromManager(_geoFenceManager->breachReturnPoint());
//        _setFenceFromManager(_geoFenceManager->polygons(), _geoFenceManager->circles());
//        setDirty(false);
//        _signalAll();
//        emit loadComplete();
//    }
//    _itemsRequested = false;
}

void SkyyLedgerController::_managerSendComplete(bool error)
{
    // Fly view always reloads on manager sendComplete
    if (!error && _flyView) {
        showPlanFromManagerVehicle();
    }
}

void SkyyLedgerController::_managerRemoveAllComplete(bool error)
{
    if (!error) {
        // Remove all from vehicle so we always update
        showPlanFromManagerVehicle();
    }
}

bool SkyyLedgerController::containsItems(void) const
{
    return _polygons.count() > 0 || _circles.count() > 0;
}

void SkyyLedgerController::_updateContainsItems(void)
{
    emit containsItemsChanged(containsItems());
}

bool SkyyLedgerController::showPlanFromManagerVehicle(void)
{
    qCDebug(SkyyLedgerControllerLog) << "showPlanFromManagerVehicle _flyView" << _flyView;
    if (_masterController->offline()) {
        qCWarning(SkyyLedgerControllerLog) << "SkyyLedgerController::showPlanFromManagerVehicle called while offline";
        return true;    // stops further propagation of showPlanFromManagerVehicle due to error
    } else {
        _itemsRequested = true;
        if (!_managerVehicle->initialPlanRequestComplete()) {
            // The vehicle hasn't completed initial load, we can just wait for loadComplete to be signalled automatically
            qCDebug(SkyyLedgerControllerLog) << "showPlanFromManagerVehicle: !initialPlanRequestComplete, wait for signal";
            return true;
        } else if (syncInProgress()) {
            // If the sync is already in progress, _loadComplete will be called automatically when it is done. So no need to do anything.
            qCDebug(SkyyLedgerControllerLog) << "showPlanFromManagerVehicle: syncInProgress wait for signal";
            return true;
        } else {
            // Fake a _loadComplete with the current items
            qCDebug(SkyyLedgerControllerLog) << "showPlanFromManagerVehicle: sync complete simulate signal";
            _itemsRequested = true;
            _managerLoadComplete();
            return false;
        }
    }
}

void SkyyLedgerController::addInclusionPolygon(QGeoCoordinate topLeft, QGeoCoordinate bottomRight)
{
    QGeoCoordinate topRight(topLeft.latitude(), bottomRight.longitude());
    QGeoCoordinate bottomLeft(bottomRight.latitude(), topLeft.longitude());

    double halfWidthMeters = topLeft.distanceTo(topRight) / 2.0;
    double halfHeightMeters = topLeft.distanceTo(bottomLeft) / 2.0;

    QGeoCoordinate centerLeftEdge = topLeft.atDistanceAndAzimuth(halfHeightMeters, 180);
    QGeoCoordinate centerTopEdge = topLeft.atDistanceAndAzimuth(halfWidthMeters, 90);
    QGeoCoordinate center(centerLeftEdge.latitude(), centerTopEdge.longitude());

    // Initial polygon is inset to take 3/4s of viewport with max width/height of 3000 meters
    halfWidthMeters =   qMin(halfWidthMeters * 0.75, 1500.0);
    halfHeightMeters =  qMin(halfHeightMeters * 0.75, 1500.0);

    // Initial polygon has max width and height of 3000 meters
    topLeft =           center.atDistanceAndAzimuth(halfWidthMeters, -90).atDistanceAndAzimuth(halfHeightMeters, 0);
    topRight =          center.atDistanceAndAzimuth(halfWidthMeters, 90).atDistanceAndAzimuth(halfHeightMeters, 0);
    bottomLeft =        center.atDistanceAndAzimuth(halfWidthMeters, -90).atDistanceAndAzimuth(halfHeightMeters, 180);
    bottomRight =       center.atDistanceAndAzimuth(halfWidthMeters, 90).atDistanceAndAzimuth(halfHeightMeters, 180);

    QGCSkyyPolygon* polygon = new QGCSkyyPolygon(true /* inclusion */, this);
    polygon->appendVertex(topLeft);
    polygon->appendVertex(topRight);
    polygon->appendVertex(bottomRight);
    polygon->appendVertex(bottomLeft);
    _polygons.append(polygon);

    clearAllInteractive();
    polygon->setInteractive(true);
}

void SkyyLedgerController::addInclusionCircle(QGeoCoordinate topLeft, QGeoCoordinate bottomRight)
{
    QGeoCoordinate topRight(topLeft.latitude(), bottomRight.longitude());
    QGeoCoordinate bottomLeft(bottomRight.latitude(), topLeft.longitude());

    // Initial radius is inset to take 3/4s of viewport and max of 1500 meters
    double halfWidthMeters = topLeft.distanceTo(topRight) / 2.0;
    double halfHeightMeters = topLeft.distanceTo(bottomLeft) / 2.0;
    double radius = qMin(qMin(halfWidthMeters, halfHeightMeters) * 0.75, 1500.0);

    QGeoCoordinate centerLeftEdge = topLeft.atDistanceAndAzimuth(halfHeightMeters, 180);
    QGeoCoordinate centerTopEdge = topLeft.atDistanceAndAzimuth(halfWidthMeters, 90);
    QGeoCoordinate center(centerLeftEdge.latitude(), centerTopEdge.longitude());

    QGCSkyyCircle* circle = new QGCSkyyCircle(center, radius, true /* inclusion */, this);
    _circles.append(circle);

    clearAllInteractive();
    circle->setInteractive(true);
}

void SkyyLedgerController::deletePolygon(int index)
{
    if (index < 0 || index > _polygons.count() - 1) {
        return;
    }

    QGCSkyyPolygon* polygon = qobject_cast<QGCSkyyPolygon*>(_polygons.removeAt(index));
    polygon->deleteLater();
}

void SkyyLedgerController::deleteCircle(int index)
{
    if (index < 0 || index > _circles.count() - 1) {
        return;
    }

    QGCSkyyCircle* circle = qobject_cast<QGCSkyyCircle*>(_circles.removeAt(index));
    circle->deleteLater();
}

void SkyyLedgerController::clearAllInteractive(void)
{
    for (int i=0; i<_polygons.count(); i++) {
        _polygons.value<QGCSkyyPolygon*>(i)->setInteractive(false);
    }
    for (int i=0; i<_circles.count(); i++) {
        _circles.value<QGCSkyyCircle*>(i)->setInteractive(false);
    }
}

bool SkyyLedgerController::supported(void) const
{
    return (_managerVehicle->capabilityBits() & MAV_PROTOCOL_CAPABILITY_MISSION_FENCE) && (_managerVehicle->maxProtoVersion() >= 200);
}

// Hack for PX4
double SkyyLedgerController::paramCircularFence(void)
{
    if (_managerVehicle->isOfflineEditingVehicle() || !_managerVehicle->parameterManager()->parameterExists(FactSystem::defaultComponentId, _px4ParamCircularFence)) {
        return 0;
    }

    return _managerVehicle->parameterManager()->getParameter(FactSystem::defaultComponentId, _px4ParamCircularFence)->rawValue().toDouble();
}

void SkyyLedgerController::_parametersReady(void)
{
    if (_px4ParamCircularFenceFact) {
        _px4ParamCircularFenceFact->disconnect(this);
        _px4ParamCircularFenceFact = NULL;
    }

    if (_managerVehicle->isOfflineEditingVehicle() || !_managerVehicle->parameterManager()->parameterExists(FactSystem::defaultComponentId, _px4ParamCircularFence)) {
        emit paramCircularFenceChanged();
        return;
    }

    _px4ParamCircularFenceFact = _managerVehicle->parameterManager()->getParameter(FactSystem::defaultComponentId, _px4ParamCircularFence);
    connect(_px4ParamCircularFenceFact, &Fact::rawValueChanged, this, &SkyyLedgerController::paramCircularFenceChanged);
    emit paramCircularFenceChanged();
}
