/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

#include "QGCSkyyPolygon.h"
#include "JsonHelper.h"
#include <QDebug>
#include <QJsonDocument>

const char* QGCSkyyPolygon::_jsonInclusionKey = "inclusion";
const char* QGCSkyyPolygon::_jsonTypeKey = "type";
const char* QGCSkyyPolygon::_jsonMetaDataKey = "metadata";
const char* QGCSkyyPolygon::_jsonNameKey = "name";
const char* QGCSkyyPolygon::_jsonMinAltKey = "minAlt";

QGCSkyyPolygon::QGCSkyyPolygon(bool inclusion, QObject* parent)
    : QGCMapPolygon (parent)
    , _inclusion    (inclusion)
{
    _init();
}

QGCSkyyPolygon::QGCSkyyPolygon(const QGCSkyyPolygon& other, QObject* parent)
    : QGCMapPolygon (other, parent)
    , _inclusion    (other._inclusion)
{
    _init();
}

void QGCSkyyPolygon::_init(void)
{
    connect(this, &QGCSkyyPolygon::inclusionChanged, this, &QGCSkyyPolygon::_setDirty);
}

const QGCSkyyPolygon& QGCSkyyPolygon::operator=(const QGCSkyyPolygon& other)
{
    QGCMapPolygon::operator=(other);

    setInclusion(other._inclusion);

    return *this;
}

void QGCSkyyPolygon::_setDirty(void)
{
    setDirty(true);
}

void QGCSkyyPolygon::saveToJson(QJsonObject& json)
{
    json[JsonHelper::jsonVersionKey] = _jsonCurrentVersion;
    json[_jsonInclusionKey] = _inclusion;
    QGCMapPolygon::saveToJson(json);
}

bool QGCSkyyPolygon::loadFromJson(const QJsonObject& json, bool required, QString& errorString)
{
    errorString.clear();

    if (!QGCMapPolygon::loadFromJson(json, required, errorString)) {
        return false;
    }

    QString defType = "no-fly";
    if (json.contains(_jsonTypeKey))
        defType = json[_jsonTypeKey].toString();

    QString defName = defType;
    QString defMetaData = "";
    if (json.contains(_jsonMetaDataKey)) {
        QJsonObject newObj = json[_jsonMetaDataKey].toObject();

        if(newObj.contains(_jsonNameKey))
            defName = newObj[_jsonNameKey].toString();

        QJsonDocument doc(newObj);
        defMetaData = doc.toJson(QJsonDocument::Compact);
    }

    int defMinAlt = 0;
    if (json.contains(_jsonMinAltKey))
        defMinAlt = json[_jsonMinAltKey].toInt();

    setMinAlt(defMinAlt);
    setType(defType);
    setName(defName);
    setMetaData(defMetaData);
    setArea(QGCMapPolygon::area());

    return true;
}

void QGCSkyyPolygon::setInclusion(bool inclusion)
{
    if (inclusion != _inclusion) {
        _inclusion = inclusion;
        emit inclusionChanged(inclusion);
    }
}

void QGCSkyyPolygon::setType(QString type)
{
    if (type != _type) {
        _type = type;
        emit typeChanged(type);
    }
}

void QGCSkyyPolygon::setName(QString name)
{
    if (name != _name) {
        _name = name;
        emit nameChanged(name);
    }
}

void QGCSkyyPolygon::setMetaData(QString metaData)
{
    if (metaData != _metaData) {
        _metaData = metaData;
        emit metaDataChanged(metaData);
    }
}

void QGCSkyyPolygon::setMinAlt(int minAlt)
{
    if (minAlt != _minAlt) {
        _minAlt = minAlt;
        emit minAltChanged(minAlt);
    }
}

void QGCSkyyPolygon::setArea(double area)
{
    if (area != _area) {
        _area = area;
        emit areaChanged(area);
    }
}