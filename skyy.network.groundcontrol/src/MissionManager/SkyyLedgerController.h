/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

#ifndef SkyyLedgerController_H
#define SkyyLedgerController_H

#include "PlanElementController.h"
#include "GeoFenceManager.h"
#include "QGCSkyyPolygon.h"
#include "QGCSkyyCircle.h"
#include "Vehicle.h"
#include "MultiVehicleManager.h"
#include "QGCLoggingCategory.h"

Q_DECLARE_LOGGING_CATEGORY(SkyyLedgerControllerLog)

class GeoFenceManager;

class SkyyLedgerController : public PlanElementController
{
    Q_OBJECT
    
public:
    SkyyLedgerController(PlanMasterController* masterController, QObject* parent = NULL);
    ~SkyyLedgerController();

    Q_PROPERTY(QmlObjectListModel*  polygons            READ polygons                                       CONSTANT)
    Q_PROPERTY(QmlObjectListModel*  allPolygons         READ allPolygons                                    CONSTANT)
    Q_PROPERTY(QmlObjectListModel*  ledgerPolygons      READ ledgerPolygons                                 CONSTANT)
    Q_PROPERTY(QmlObjectListModel*  pendingPolygons     READ pendingPolygons                                CONSTANT)
    Q_PROPERTY(QmlObjectListModel*  circles             READ circles                                        CONSTANT)
    Q_PROPERTY(QmlObjectListModel*  allCircles          READ allCircles                                     CONSTANT)
    Q_PROPERTY(QmlObjectListModel*  ledgerCircles       READ ledgerCircles                                  CONSTANT)
    Q_PROPERTY(QmlObjectListModel*  pendingCircles      READ pendingCircles                                 CONSTANT)
    Q_PROPERTY(QGeoCoordinate       breachReturnPoint   READ breachReturnPoint  WRITE setBreachReturnPoint  NOTIFY breachReturnPointChanged)

    // Hack to expose PX4 circular fence controlled by GF_MAX_HOR_DIST
    Q_PROPERTY(double               paramCircularFence  READ paramCircularFence                             NOTIFY paramCircularFenceChanged)

    /// Add a new inclusion polygon to the fence
    ///     @param topLeft - Top left coordinate or map viewport
    ///     @param topLeft - Bottom right left coordinate or map viewport
    Q_INVOKABLE void addInclusionPolygon(QGeoCoordinate topLeft, QGeoCoordinate bottomRight);

    /// Add a new inclusion circle to the fence
    ///     @param topLeft - Top left coordinate or map viewport
    ///     @param topLeft - Bottom right left coordinate or map viewport
    Q_INVOKABLE void addInclusionCircle(QGeoCoordinate topLeft, QGeoCoordinate bottomRight);

    /// Deletes the specified polygon from the polygon list
    ///     @param index Index of poygon to delete
    Q_INVOKABLE void deletePolygon(int index);

    /// Deletes the specified circle from the circle list
    ///     @param index Index of circle to delete
    Q_INVOKABLE void deleteCircle(int index);

    /// Clears the interactive bit from all fence items
    Q_INVOKABLE void clearAllInteractive(void);

    double paramCircularFence(void);

    // Overrides from PlanElementController
    bool supported                  (void) const final;
    void start                      (bool flyView) final;
    void save                       (QJsonObject& json) final;
    bool load                       (const QJsonObject& json, QString& errorString) final;
    QStringList getAvailableTypes   (void);
    bool load                       (const QJsonObject& json, QString type, QString& errorString);
    void loadFromVehicle            (void) final;
    void sendToVehicle              (void) final;
    void removeAll                  (void) final;
    void removeAllFromVehicle       (void) final;
    bool syncInProgress             (void) const final;
    bool dirty                      (void) const final;
    void setDirty                   (bool dirty) final;
    bool containsItems              (void) const final;
    void managerVehicleChanged      (Vehicle* managerVehicle) final;
    bool showPlanFromManagerVehicle (void) final;

    QmlObjectListModel* polygons                (void) { return &_polygons; }
    QmlObjectListModel* allPolygons             (void) { return &_allPolygons; }
    QmlObjectListModel* ledgerPolygons          (void) { return &_ledgerPolygons; }
    QmlObjectListModel* pendingPolygons         (void) { return &_pendingPolygons; }
    QmlObjectListModel* circles                 (void) { return &_circles; }
    QmlObjectListModel* allCircles              (void) { return &_allCircles; }
    QmlObjectListModel* ledgerCircles           (void) { return &_ledgerCircles; }
    QmlObjectListModel* pendingCircles          (void) { return &_pendingCircles; }
    QGeoCoordinate      breachReturnPoint       (void) const { return _breachReturnPoint; }

    void setBreachReturnPoint(const QGeoCoordinate& breachReturnPoint);

signals:
    void breachReturnPointChanged       (QGeoCoordinate breachReturnPoint);
    void editorQmlChanged               (QString editorQml);
    void loadComplete                   (void);
    void paramCircularFenceChanged      (void);

private slots:
    void _polygonDirtyChanged       (bool dirty);
    void _setDirty                  (void);
    void _setFenceFromManager       (const QList<QGCFencePolygon>& polygons, const QList<QGCFenceCircle>&  circles);
    void _setReturnPointFromManager (QGeoCoordinate breachReturnPoint);
    void _managerLoadComplete       (void);
    void _updateContainsItems       (void);
    void _managerSendComplete       (bool error);
    void _managerRemoveAllComplete  (bool error);
    void _parametersReady           (void);

private:
    void _init(void);
    void _signalAll(void);

    GeoFenceManager*    _geoFenceManager;
    bool                _dirty;
    QmlObjectListModel  _polygons;
    QmlObjectListModel  _allPolygons;
    QmlObjectListModel  _ledgerPolygons;
    QmlObjectListModel  _pendingPolygons;
    QmlObjectListModel  _circles;
    QmlObjectListModel  _allCircles;
    QmlObjectListModel  _ledgerCircles;
    QmlObjectListModel  _pendingCircles;
    QGeoCoordinate      _breachReturnPoint;
    bool                _itemsRequested;
    Fact*               _px4ParamCircularFenceFact;

    static const char* _px4ParamCircularFence;

    static const int _jsonCurrentVersion = 2;

    static const char* _jsonFileTypeValue;
    static const char* _jsonBreachReturnKey;
    static const char* _jsonPolygonsKey;
    static const char* _jsonCirclesKey;
};

#endif
