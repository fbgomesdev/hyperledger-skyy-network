/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

import QtQuick          2.5
import QtQuick.Controls 1.2
import QtQuick.Layouts  1.2

import QGroundControl               1.0
import QGroundControl.ScreenTools   1.0
import QGroundControl.Controls      1.0

Rectangle {
    id:         root
    radius:     ScreenTools.defaultFontPixelWidth * 0.5
    color:      qgcPal.colorGreen

    Component.onCompleted: {
        getPendingAirspaces()
    }

    property var payload:    ""
    property var maxHeight
    property var masterController
    property var mapControl

    onPayloadChanged: calculateNewHeight()

    onMaxHeightChanged: calculateNewHeight()

    function calculateNewHeight() {
        var itemsCount = payload == null ? 0 : Object.keys(payload).length

        if(itemsCount == 0) {
            noPendingRequests.visible = true
            root.height = 130
        } else {
            noPendingRequests.visible = false
            var newHeight = 155 * itemsCount + 90
            root.height = newHeight > maxHeight ? maxHeight : newHeight
        }
    }

    function getPendingAirspaces() {
        var ip = QGroundControl.settingsManager.appSettings.skyyNetworkAddress.value
        var xmlhttp = new XMLHttpRequest();
        var url = "http://" + ip + "/fabric/getAirspacesToBeApproved";

        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
                var originalJson = JSON.parse(xmlhttp.responseText)
                payload = originalJson.payload
                displayPendingAirspaces(payload, "", "skyy-ledger")
            }
        }

        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    function aproveAirSpace(approved, index) {
        var ip = QGroundControl.settingsManager.appSettings.skyyNetworkAddress.value
        var xmlhttp = new XMLHttpRequest();
        var url = "http://" + ip + "/fabric/rejectAirspace?id=" + payload[index].id;
        if(approved)
            url = "http://" + ip + "/fabric/approveAirspace?id=" + payload[index].id;

        xmlhttp.open("POST", url, true);

        xmlhttp.setRequestHeader("Content-Type", "application/json");
        xmlhttp.onreadystatechange = function() { // Call a function when the state changes.
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    // mark the selected airspace as approved
                    displayPendingAirspaces(payload, payload[index].id, approved ? "skyy-ledger" : "deleted")

                    // refresh listview with new values
                    payload.splice(index, 1);
                    payload = JSON.parse(JSON.stringify(payload));
                }
            }
        }
        xmlhttp.send();
    }

    function displayPendingAirspaces(json, id, newType) {
        var centerLat = 0
        var centerLong = 0

        var circles = JSON.parse("[]")
        var polygons = JSON.parse("[]")
        for (var generalIndex in json) {
            var meinMetaData = json[generalIndex].skyyLedgerData
            var currentFlyspace = json[generalIndex].flyspace
            for (var index in currentFlyspace.circles) {
                var currentCircle = currentFlyspace.circles[index];

                if(id == json[generalIndex].id) {
                    centerLat = currentCircle.circle.center[0]
                    centerLong = currentCircle.circle.center[1]
                    currentCircle.type = newType
                } else {
                    currentCircle.type = "pending"
                }

                var metaData = {}
                metaData.name = meinMetaData.operatorName
                metaData.certificate = meinMetaData.certificate
                metaData.droneType = meinMetaData.droneType
                metaData.transactionId = meinMetaData.transactionId

                currentCircle.metadata = metaData

                circles.push(currentCircle);
            }

            for (var index in currentFlyspace.polygons) {
                var currentPolygonObj = currentFlyspace.polygons[index];

                if(id == json[generalIndex].id) {
                    // calculate the center of the polygone
                    var minX, maxX, minY, maxY
                    for (var i in currentPolygonObj.polygon) {
                        var polygon = currentPolygonObj.polygon[i]
                        minX = (polygon[0] < minX || minX == null) ? polygon[0] : minX;
                        maxX = (polygon[0] > maxX || maxX == null) ? polygon[0] : maxX;
                        minY = (polygon[1] < minY || minY == null) ? polygon[1] : minY;
                        maxY = (polygon[1] > maxY || maxY == null) ? polygon[1] : maxY;
                    }
                    // save the center of the polygon
                    centerLat = (minX + maxX) / 2
                    centerLong = (minY + maxY) / 2;

                    currentPolygonObj.type = newType
                } else {
                    currentPolygonObj.type = "pending"
                }

                var metaData = {}
                metaData.name = meinMetaData.operatorName
                metaData.certificate = meinMetaData.certificate
                metaData.droneType = meinMetaData.droneType
                metaData.transactionId = meinMetaData.transactionId

                currentPolygonObj.metadata = metaData

                polygons.push(currentPolygonObj);
            }
        }
        var finalObject = {}
        var flyspace = {}
        flyspace.circles = circles
        flyspace.polygons = polygons
        finalObject.flyspace = flyspace

        masterController.loadFromJsonObj(JSON.stringify(finalObject), "pending");

        // focus map on polygon
        if(centerLong != 0 && centerLat != 0 && newType == "active") {
            mapControl.center.longitude = centerLong
            mapControl.center.latitude = centerLat
        }
    }

    function trunkString(originalString) {
        return "TrxID: " + originalString.substring(0,10) + ".." + originalString.substring(originalString.length -11, originalString.length -1)
    }

    QGCLabel {
        id:                     title
        anchors.top:            parent.top
        anchors.leftMargin:     10
        anchors.left:           root.left
        anchors.topMargin:      5
        color:                  "black"
        text:                   "Regulator Interface"
        font.family:            ScreenTools.demiboldFontFamily
        font.pointSize:         15
    }

    QGCLabel {
        id:                    noPendingRequests
        anchors.top:           title.bottom
        anchors.leftMargin:    10
        anchors.left:          root.left
        anchors.topMargin:     10
        color:                 "black"
        text:                  "There are no pending requests"
    }

    QGCListView {
        id:                    listView1
        model:                 payload
        width:                 parent.width - 20
        height:                parent.height - 100
        anchors.top:           title.bottom
        anchors.leftMargin:    10
        anchors.left:          root.left
        anchors.topMargin:     10
        spacing:               10
        clip:                  true
        delegate:              Rectangle {

            width:             parent.width
            height:            145
            radius:            ScreenTools.defaultFontPixelWidth * 0.5
            color:             qgcPal.windowShadeDark

            QGCLabel {
                id:                     operatorNameId
                anchors.top:            parent.top
                anchors.leftMargin:     5
                anchors.left:           parent.left
                anchors.topMargin:      3
                text:                   payload[index].skyyLedgerData.operatorName
            }

            QGCLabel {
                id:                     typeAndCert
                anchors.top:            operatorNameId.bottom
                anchors.leftMargin:     5
                anchors.left:           parent.left
                anchors.topMargin:      2
                text:                   payload[index].skyyLedgerData.droneType + ", " + payload[index].skyyLedgerData.certificate
            }

            QGCLabel {
                id:                     altitude
                anchors.top:            typeAndCert.bottom
                anchors.leftMargin:     5
                anchors.left:           parent.left
                anchors.topMargin:      2
                text:                   "Altitude from " + payload[index].skyyLedgerData.altitude.min + "m to " + payload[index].skyyLedgerData.altitude.max + "m"
            }

            QGCLabel {
                id:                     blockId
                anchors.top:            altitude.bottom
                anchors.leftMargin:     5
                anchors.left:           parent.left
                anchors.topMargin:      2
                text:                   trunkString(payload[index].skyyLedgerData.transactionId)
            }

            QGCButton {
                id:                     showBtn
                anchors.top:            blockId.bottom
                anchors.leftMargin:     5
                anchors.left:           parent.left
                anchors.topMargin:      5
                width:                  60
                height:                 30
                text:                   "Show"
                onClicked: {
                    displayPendingAirspaces(payload, payload[index].id, "active")
                }
            }

            QGCButton {
                id:                     approveBtn
                anchors.top:            blockId.bottom
                anchors.leftMargin:     5
                anchors.left:           showBtn.right
                anchors.topMargin:      5
                width:                  75
                height:                 30
                text:                   "Approve"
                onClicked: {
                    aproveAirSpace(true, index)
                }
            }

            QGCButton {
                id:                     rejectBtn
                anchors.top:            blockId.bottom
                anchors.leftMargin:     5
                anchors.left:           approveBtn.right
                anchors.topMargin:      5
                width:                  75
                height:                 30
                text:                   "Reject"
                onClicked: {
                    aproveAirSpace(false, index)
                }
            }
        }
    }

    QGCButton {
        id:                       refreshBtn
        anchors.bottom:           parent.bottom
        anchors.leftMargin:       10
        anchors.left:             root.left
        anchors.bottomMargin:     10
        anchors.horizontalCenter: parent.horizontalCenter
        text:                     "Refresh"
        onClicked: {
            getPendingAirspaces()
        }
    }
}


