/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

import QtQuick          2.5
import QtQuick.Controls 1.2
import QtQuick.Layouts  1.2

import QGroundControl               1.0
import QGroundControl.ScreenTools   1.0
import QGroundControl.Controls      1.0

Rectangle {
    id:         root
    radius:     ScreenTools.defaultFontPixelWidth * 0.5
    color:      qgcPal.window
    width:      operatorNameId.width + 25
    height:     operatorNameId.height - 10
    opacity:    0.80
    clip:       true

    property var metaDataText

    QGCLabel {
        id:                     operatorNameId
        anchors.top:            parent.top
        anchors.leftMargin:     10
        anchors.left:           root.left
        anchors.topMargin:      5
        text:                   metaDataText
    }
}