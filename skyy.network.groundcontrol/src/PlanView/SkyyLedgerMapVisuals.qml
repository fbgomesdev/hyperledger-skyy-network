/****************************************************************************
 *
 *   (c) 2009-2016 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/

import QtQuick          2.3
import QtQuick.Controls 1.2
import QtLocation       5.3
import QtPositioning    5.3

import QGroundControl               1.0
import QGroundControl.ScreenTools   1.0
import QGroundControl.Palette       1.0
import QGroundControl.Controls      1.0
import QGroundControl.FlightMap     1.0

/// GeoFence map visuals
Item {
    z: QGroundControl.zOrderMapItems

    property var    map
    property var    skyLedgerController
    property bool   interactive:            false   ///< true: user can interact with items
    property bool   planView:               false   ///< true: visuals showing in plan view
    property var    homePosition

    //property var    _breachReturnPointComponent
    //property var    _mouseAreaComponent
    property var    _paramCircleFenceComponent
    property var    _polygons
    property var    _circles
    property color  _borderColor:               "red"
    property int    _borderWidthInclusion:      2
    property int    _borderWidthExclusion:      0
    property color  _interiorColorExclusion:    "orange"
    property color  _interiorColorInclusion:    "transparent"
    property real   _interiorOpacityExclusion:  0.2
    property real   _interiorOpacityInclusion:  1

    function addPolygon(inclusionPolygon) {
        // Initial polygon is inset to take 2/3rds space
        var rect = Qt.rect(map.centerViewport.x, map.centerViewport.y, map.centerViewport.width, map.centerViewport.height)
        rect.x += (rect.width * 0.25) / 2
        rect.y += (rect.height * 0.25) / 2
        rect.width *= 0.75
        rect.height *= 0.75

        var centerCoord =       map.toCoordinate(Qt.point(rect.x + (rect.width / 2), rect.y + (rect.height / 2)),   false /* clipToViewPort */)
        var topLeftCoord =      map.toCoordinate(Qt.point(rect.x, rect.y),                                          false /* clipToViewPort */)
        var topRightCoord =     map.toCoordinate(Qt.point(rect.x + rect.width, rect.y),                             false /* clipToViewPort */)
        var bottomLeftCoord =   map.toCoordinate(Qt.point(rect.x, rect.y + rect.height),                            false /* clipToViewPort */)
        var bottomRightCoord =  map.toCoordinate(Qt.point(rect.x + rect.width, rect.y + rect.height),               false /* clipToViewPort */)

        // Initial polygon has max width and height of 3000 meters
        var halfWidthMeters =   Math.min(topLeftCoord.distanceTo(topRightCoord), 3000) / 2
        var halfHeightMeters =  Math.min(topLeftCoord.distanceTo(bottomLeftCoord), 3000) / 2
        topLeftCoord =      centerCoord.atDistanceAndAzimuth(halfWidthMeters, -90).atDistanceAndAzimuth(halfHeightMeters, 0)
        topRightCoord =     centerCoord.atDistanceAndAzimuth(halfWidthMeters, 90).atDistanceAndAzimuth(halfHeightMeters, 0)
        bottomLeftCoord =   centerCoord.atDistanceAndAzimuth(halfWidthMeters, -90).atDistanceAndAzimuth(halfHeightMeters, 180)
        bottomRightCoord =  centerCoord.atDistanceAndAzimuth(halfWidthMeters, 90).atDistanceAndAzimuth(halfHeightMeters, 180)

        console.log(map.center)
        console.log(topLeftCoord)
        console.log(bottomRightCoord)

        if (inclusionPolygon) {
            skyLedgerController.addInclusion(topLeftCoord, bottomRightCoord)
        } else {
            skyLedgerController.addExclusion(topLeftCoord, bottomRightCoord)
        }

        console.log(object.type)
        console.log(object)
    }

    function getColorFromType (type) {
        switch(type) {
            case "no-fly":
                return "#EB984E" // portocaliu
            case "pending":
                return "#76448A" // mov
            case "active":
                return qgcPal.colorBlue // albastru
            case "skyy-ledger":
                return "#2ECC71" // verde
            case "Prohibited":
                return "#F1948A" // roz
            case "Restricted":
                return "#922B21" // visiniu
            case "Danger":
                return "#E74C3C" // rosu
            case "CTA":
                return "#48C9B0" // verde deschis
            case "Warning":
                return "#1E8449" // verde inchis
            case "OtherSUAS":
                return "#9A7D0A" // cacaniu
            case "MOA":
                return "#F4D03F" // galben
            case "ADIZ":
                return "#ffffff" // alb
            case "OCA":
                return "#C39BD3" // mov deschis
            case "FuelDumpingArea":
                return "#2980B9" // albastru
            case "CTR":
                return "#85C1E9" // albastru deschis
            case "Alert":
                return "#CACFD2" // gri
            case "TsaTraCba":
                return "#5D6D7E" // gri inchis
            case "TMA":
                return "#33FFF9" // tzipator
            default:
                return "black"
        }
    }

    Component.onCompleted: {
        //_breachReturnPointComponent = breachReturnPointComponent.createObject(map)
        //map.addMapItem(_breachReturnPointComponent)
        //_mouseAreaComponent = mouseAreaComponent.createObject(map)
        _paramCircleFenceComponent = paramCircleFenceComponent.createObject(map)
        map.addMapItem(_paramCircleFenceComponent)
    }

    Component.onDestruction: {
        //_breachReturnPointComponent.destroy()
        //_mouseAreaComponent.destroy()
        _paramCircleFenceComponent.destroy()
    }

    // Mouse area to capture breach return point coordinate
    Component {
        id: mouseAreaComponent

        MouseArea {
            anchors.fill:   map
            visible:        interactive
            onClicked:      skyLedgerController.breachReturnPoint = map.toCoordinate(Qt.point(mouse.x, mouse.y), false /* clipToViewPort */)
        }
    }

    Instantiator {
        model: _polygons

        delegate : QGCMapPolygonVisuals {
            mapControl:         map
            mapPolygon:         object
            borderWidth:        5
            borderColor:        getColorFromType(object.type)
            interiorColor:      _interiorColorInclusion
            interiorOpacity:    _interiorOpacityInclusion
            label:              object.name + "\n" + object.type
            metaData:           object.metaData
        }
    }

    Instantiator {
        model: _circles

        delegate : QGCMapCircleVisuals {
            mapControl:         map
            mapCircle:          object
            borderWidth:        5
            borderColor:        getColorFromType(object.type)
            interiorColor:      _interiorColorInclusion
            interiorOpacity:    _interiorOpacityInclusion
            label:              object.name + "\n" + object.type
            metaData:           object.metaData
        }
    }

    // Circular geofence specified from parameter
    Component {
        id: paramCircleFenceComponent

        MapCircle {
            color:          _interiorColorInclusion
            opacity:        _interiorOpacityInclusion
            border.color:   _borderColor
            border.width:   _borderWidthInclusion
            center:         homePosition
            radius:         _radius
            visible:        homePosition.isValid && _radius > 0

            property real _radius: skyLedgerController.paramCircularFence

            on_RadiusChanged: console.log("_radius", _radius, homePosition.isValid, homePosition)
        }
    }

    // Breach return point
    Component {
        id: breachReturnPointComponent

        MapQuickItem {
            anchorPoint.x:  sourceItem.anchorPointX
            anchorPoint.y:  sourceItem.anchorPointY
            z:              QGroundControl.zOrderMapItems
            coordinate:     skyLedgerController.breachReturnPoint

            sourceItem: MissionItemIndexLabel {
                label: "B"
            }
        }
    }
}
