import QtQuick          2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts  1.2

import QGroundControl               1.0
import QGroundControl.ScreenTools   1.0
import QGroundControl.Controls      1.0

QGCFlickable {
    height:         outerEditorRect.height
    contentHeight:  outerEditorRect.height
    clip:           true

    property var controller ///< GeoFenceController
    property var lastRequestId

    readonly property real  _margin: ScreenTools.defaultFontPixelWidth / 2
    readonly property real  _radius: ScreenTools.defaultFontPixelWidth / 2

    function validateRequest() {
        var hasErrors = false;
        if(operatorName.text == "") {
            hasErrors = true;
            operatorNameLabel.color = qgcPal.colorRed
        } else {
            operatorNameLabel.color = qgcPal.text
        }

        if(certificate.text == "") {
            hasErrors = true;
            certificateLabel.color = qgcPal.colorRed
        } else {
            certificateLabel.color = qgcPal.text
        }

        if(droneType.text == "") {
            hasErrors = true;
            droneTypeLabel.color = qgcPal.colorRed
        } else {
            droneTypeLabel.color = qgcPal.text
        }

        if(droneType.text == "") {
            hasErrors = true;
            droneTypeLabel.color = qgcPal.colorRed
        } else {
            droneTypeLabel.color = qgcPal.text
        }

        if(altitudeMax.text == "") {
            hasErrors = true;
            altitudeMaxLabel.color = qgcPal.colorRed
        } else {
            altitudeMaxLabel.color = qgcPal.text
        }

        if(altitudeMin.text == "") {
            hasErrors = true;
            altitudeMinLabel.color = qgcPal.colorRed
        } else {
            altitudeMinLabel.color = qgcPal.text
        }

        if(!hasErrors) {
            getGeoFenceData()
        }
    }

    function getGeoFenceData() {
        var json = controller.getAllFences()
        var geoFenceObject = JSON.parse(json);

        if(geoFenceObject.circles.length == 0 && geoFenceObject.polygons.length == 0) {
            updateInterface("failed", "You need first to define a fence, \n before you submit it.")
        } else {
            // add proper type for each airspace
            if(geoFenceObject.circles.length > 0) {
                for (var index in geoFenceObject.circles) {
                    geoFenceObject.circles[index].type = "pending"
                }
            }

            if(geoFenceObject.polygons.length > 0) {
                for (var index in geoFenceObject.polygons) {
                    geoFenceObject.polygons[index].type = "pending"
                }
            }

            var skyLedgerData = {}
            // operator name and cerificate, and drone type
            skyLedgerData.operatorName = operatorName.text
            skyLedgerData.certificate = certificate.text
            skyLedgerData.droneType = droneType.text

            // altitude
            var altitude = {}
            altitude.max = altitudeMax.text
            altitude.min = altitudeMin.text
            altitude.unit = "m"
            skyLedgerData.altitude = altitude

            // duration
            var duration = {}
            duration.count = 120
            duration.unit = "m"
            skyLedgerData.duration = duration

            var finalRequestJson = {}

            finalRequestJson.skyyLedgerData = skyLedgerData
            finalRequestJson.flyspace = geoFenceObject

            skyyLedgerRequestCall(JSON.stringify(finalRequestJson))
        }
    }

    function skyyLedgerRequestCall(json) {
        var ip = QGroundControl.settingsManager.appSettings.skyyNetworkAddress.value
        var xmlhttp = new XMLHttpRequest();
        var url = "http://" + ip + "/fabric/registerAirspace";
        xmlhttp.open("POST", url, true);

        xmlhttp.setRequestHeader("Content-Type", "application/json");
        xmlhttp.onreadystatechange = function() { // Call a function when the state changes.
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    console.log(xmlhttp.responseText)
                    var json = JSON.parse(xmlhttp.responseText);
                    if(json.status == "successful") {
                        updateInterface("pending", "Airspace request submitted");

                        // save the request id
                        lastRequestId = json.payload

                        // start timer for check request status
                        checkStatusTimer.start()

                        // delete current fences
                        controller.removeAll()
                    } else {
                        updateInterface(json.status, json.message);
                    }
                } else {
                    updateInterface("failed", "Unable to send the request")
                }
            }
        }
        xmlhttp.send(json);
    }

    function checkStatusForId() {
        var ip = QGroundControl.settingsManager.appSettings.skyyNetworkAddress.value
        var xmlhttp = new XMLHttpRequest();
        var url = "http://" + ip + "/fabric/status?id=" + lastRequestId;

        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
                var originalJson = JSON.parse(xmlhttp.responseText)
                if(originalJson.payload != "pending") {
                    checkStatusTimer.stop()
                    if(originalJson.payload == "approved") {
                        updateInterface("approved", "SkyyAirspace Approved")
                    } else {
                        updateInterface("rejected", "Airspace N/A")
                    }
                }
            }
        }

        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    function updateInterface(status, message) {
        requestMsg.text = message;
        requestMsg.visible = true;
        requestMsg.color = status === "approved" ? qgcPal.colorGreen : status === "pending" ? qgcPal.colorOrange : qgcPal.colorRed;
    }

    Timer {
        id:         checkStatusTimer
        interval:   5000
        repeat:     true
        onTriggered: {
            checkStatusForId()
        }
    }

    Rectangle {
        id:     outerEditorRect
        width:  parent.width
        height: innerEditorRect.y + innerEditorRect.height + (_margin * 2)
        radius: _radius
        color:  qgcPal.colorGreen

        QGCLabel {
            id:                 editorLabel
            anchors.margins:    _margin
            anchors.left:       parent.left
            anchors.top:        parent.top
            text:               qsTr("SkyyFence")
        }

        Rectangle {
            id:                 innerEditorRect
            anchors.margins:    _margin
            anchors.left:       parent.left
            anchors.right:      parent.right
            anchors.top:        editorLabel.bottom
            height:             (_margin * 135)
            color:              qgcPal.windowShadeDark
            radius:             _radius

            QGCLabel {
                id:                 infoLabel
                anchors.margins:    _margin
                anchors.top:        parent.top
                anchors.left:       parent.left
                anchors.right:      parent.right
                wrapMode:           Text.WordWrap
                text:               qsTr("Submit your fence to Skyy Ledger for approval.")
            }

            QGCLabel {
                id:                      operatorNameLabel
                anchors.margins:         _margin * 3
                anchors.top:             infoLabel.bottom
                anchors.left:            parent.left
                anchors.right:           parent.right
                text:                    qsTr("Operator Name:")
                width:                   parent.width
            }

            QGCTextField {
                id:                      operatorName
                width:                   parent.width
                anchors.left:            parent.left
                anchors.right:           parent.right
                anchors.top:             operatorNameLabel.bottom
                anchors.margins:         _margin
            }

            QGCLabel {
                id:                      certificateLabel
                anchors.margins:         _margin * 3
                anchors.top:             operatorName.bottom
                anchors.left:            parent.left
                anchors.right:           parent.right
                text:                    qsTr("Certificate/License #:")
                width:                   parent.width
            }

            QGCTextField {
                id:                      certificate
                width:                   parent.width
                anchors.left:            parent.left
                anchors.right:           parent.right
                anchors.top:             certificateLabel.bottom
                anchors.margins:         _margin
            }

            QGCLabel {
                id:                      droneTypeLabel
                anchors.margins:         _margin * 3
                anchors.top:             certificate.bottom
                anchors.left:            parent.left
                anchors.right:           parent.right
                text:                    qsTr("Drone Type:")
                width:                   parent.width
            }

            QGCTextField {
                id:                      droneType
                width:                   parent.width
                anchors.left:            parent.left
                anchors.right:           parent.right
                anchors.top:             droneTypeLabel.bottom
                anchors.margins:         _margin
            }

            QGCLabel {
                id:                      altitudeLabel
                anchors.margins:         _margin * 3
                anchors.top:             droneType.bottom
                anchors.left:            parent.left
                anchors.right:           parent.right
                text:                    qsTr("Altitude (m)")
                width:                   parent.width
            }

            GridLayout {
                id:                      altitudeContainer
                anchors.margins:         _margin * 2
                anchors.left:            parent.left
                anchors.right:           parent.right
                anchors.top:             altitudeLabel.bottom
                columnSpacing:           ScreenTools.defaultFontPixelWidth
                rowSpacing:              _margin
                columns:                 2

                QGCLabel {
                    id:                      altitudeMinLabel
                    text:                    qsTr("Min: ")
                }

                QGCTextField {
                    id:                      altitudeMin
                }

                QGCLabel {
                    id:                      altitudeMaxLabel
                    text:                    qsTr("Max: ")
                }

                QGCTextField {
                    id:                      altitudeMax
                }
            }

            QGCLabel {
                id:                      timeMsg
                anchors.margins:         _margin * 3
                anchors.top:             altitudeContainer.bottom
                anchors.left:            parent.left
                anchors.right:           parent.right
                text:                    qsTr("Duration: 2 hours after approval")
                width:                   parent.width
            }

            QGCButton {
                id:                         submitBtn
                anchors.margins:            _margin * 3
                text:                       qsTr("Submit")
                anchors.top:                timeMsg.bottom
                anchors.horizontalCenter:   parent.horizontalCenter
                onClicked: {
                    validateRequest()
                }
            }

            QGCLabel {
                id:                      requestMsg
                width:                   parent.width
                anchors.margins:         _margin * 3
                anchors.top:             submitBtn.bottom
                anchors.left:            parent.left
                anchors.right:           parent.right
                visible:                 false
            }
        }
    }
}
